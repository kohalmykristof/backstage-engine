<?
class crudPage{

	var $itemDBE;
	var $handleSubmitErrorMessage = "";
	var $idField;
	var $itemID = 0;
	var $DBCondition;
	var $additionalDBCondition = "";
	var $DBConditionValues = [];
	var $additionalDBConditionValues = [];
	protected $DBConditionValuesMerged = [];
	var $itemName = "item";
	var $itemList;
	var $itemListTitle = "Items";
	var $itemDBEClass;
	var $itemListClass;
	var $labels = array(
		"itemCreatedSuccessfully" => "New %itemName% created successfully!",
		"itemEditedSuccessfully" => "%itemName% edited successfully!",
		"newItemTitle" => "New %itemName%",
		"editItemTitle" => "Edit %itemName%",
		"editItemTitleItemID" => ": %itemID%",
		"itemDeletedSuccessfully" => "%itemName% successfully deleted!",
		"itemDeletedError" => "Delete %itemName% is unsuccessful due to database error: ",
		"addNewItemLink" => "Add new item",
		"deleteConfirmQuestion" => "Are you sure you want to delete this item?"
	);
	var $anchor = false;
	var $showItemIdInItemTitle = false;
	var $formListOrder = true;
	var $caseImgButtons = array();
	var $editItemField;
	var $deleteItemField;
	var $caseImgHandlersOnly = false;
	var $additionalBackButtonFormValues = array();

	public function __construct($idField, $itemDBEClass, $itemName, $itemListClass, $config = array()) {
		$this -> idField = $idField;
		$this -> itemID = isset($_POST[$idField]) ? $_POST[$idField] : 0;
		$this -> itemName = $itemName;
		$this -> itemDBEClass = $itemDBEClass;
		$this -> itemListClass = $itemListClass;
		foreach ($config as $k => $e) {
			if (property_exists($this, $k)) {
				$this -> $k = $e;
			}
		}
		$this -> editItemField = "edit_".$idField;
		$this -> deleteItemField = "delete_".$idField;
		$this -> caseImgButtons[$this -> editItemField] = array(
			"default" => array(
				"handler" => function() {
					$this -> printItemForm($this -> itemID);
					$this -> caseImgHandlersOnly = true;
				},
				"img" => INTRANET_ADDRESS."/images/buttons/pencil.png",
				"title" => "Edit"
			)
		);
		$deleteConfirmQuestion = $this -> replaceCaption($this -> labels["deleteConfirmQuestion"]);
		$this -> caseImgButtons[$this -> deleteItemField] = array(
			"default" => array(
				"handler" => function() {
					$this -> deleteItem($_POST[$this -> idField]);
				},
				"img" => INTRANET_ADDRESS."/images/buttons/bin.png",
				"title" => "Delete",
				"confirmQuestion" => $deleteConfirmQuestion
			)
		);
		$this -> setClassInstances();
	}

	public function printPage() {
		foreach ($this -> caseImgButtons as $k => $e) {
			if (isset($_POST[$k])) {
				$errorFunc = function(&$errorMsg) {
					return;
				};
				if (isset($e["error"]) && is_callable($e["error"])) {
					$errorFunc = $e["error"];
				}
				$defaultFunc = function() use ($errorFunc) {
					call_user_func($errorFunc, "Missing caseImgButton value field, or case config / handler function to case '".$value."'!");
				};
				if (isset($e["default"]) && is_array($e["default"]) && isset($e["default"]["handler"]) && is_callable($e["default"]["handler"])) {
					$defaultFunc = $e["default"]["handler"];
				}
				$value = isset($e["valueField"])
					? (isset($this -> itemDBE -> values[$e["valueField"]])
						? $this -> itemDBE -> values[$e["valueField"]]
						: "")
					: "";
				if ($this -> itemID === 0) {
					call_user_func($errorFunc, "Missing itemID!");
				} else if (!isset($e["valueField"])) {
					call_user_func($defaultFunc);
				} else if (!isset($e["cases"]) || !is_array($e["cases"])) {
					call_user_func($defaultFunc);
				} else if (!isset($e["cases"][$value]) || !is_array($e["cases"][$value])) {
					call_user_func($defaultFunc);
				} else if (!isset($e["cases"][$value]["handler"]) || !is_callable($e["cases"][$value]["handler"])) {
					call_user_func($defaultFunc);
				} else {
					call_user_func($e["cases"][$value]["handler"]);
				}
				unset($_POST[$this -> idField]);
				$this -> itemID = 0;
				$this -> setClassInstances();
				break;
			}
		}

		if ($this -> caseImgHandlersOnly) {
			return;
		}
		
		if ($this -> itemDBE -> handleSubmit($this -> handleSubmitErrorMessage, (($this -> itemID === 0) ? "" : $this -> DBCondition), $this -> DBConditionValuesMerged)) {
			msgDiv($this -> replaceCaption(($this -> itemID===0) ? $this -> labels["itemCreatedSuccessfully"] : $this -> labels["itemEditedSuccessfully"]));
			print "<form action='".getUrl($_GET["page"])."' method='POST'>";
			if (is_array($this -> additionalBackButtonFormValues)) {
				foreach ($this -> additionalBackButtonFormValues as $k => $e) {
					print "<input name='$k' type='hidden' value='$e'>";
				}
			}
			print "<input type='submit' value='<< back'></form>";
		} else if (isset($_POST[$this -> itemList -> editItemField]) || $this -> itemID !== 0) {
			$this -> printItemForm($id);
		} else {
			if ($this -> anchor) {
				print "<br/><a href='#newItem'>".$this -> replaceCaption($this -> labels["addNewItemLink"])."</a><br/>";
			}
			if ($this -> formListOrder) {
				$this -> printItemForm();
				print "<br/><hr><br/>";
				$this -> printItems();
			} else {
				$this -> printItems();
				print "<br/><hr><br/>";
				$this -> printItemForm();
			}
		}
	}

	public function printItemForm() {
		$editItemTitle = $this -> labels["editItemTitle"];
		if ($this -> showItemIdInItemTitle) {
			$editItemTitle.= $this -> labels["editItemTitleItemID"];
		}
		print "<a name='newItem'><br/><h1>".$this -> replaceCaption(($this -> itemID === 0) ? ($this -> labels["newItemTitle"]) : $editItemTitle)."</h1><br/></a>";
		if ($this -> handleSubmitErrorMessage !== "") {
			errDiv($this -> handleSubmitErrorMessage);
		}
		if ($this -> itemID !== 0) {
			print "<form action='".getUrl($_GET["page"])."' method='POST'><input type='submit' value='<< back'>";
			if (is_array($this -> additionalBackButtonFormValues)) {
				foreach ($this -> additionalBackButtonFormValues as $k => $e) {
					print "<input name='$k' type='hidden' value='$e'>";
				}
			}
			print "</form><br/>";
		}
		$this -> itemDBE -> printUrlap();
	}

	public function printItems() {
		$toPrint = "<br/><h1>".$this -> itemListTitle."</h1><br/>";
		try {
			$toPrint.= $this -> itemList -> generateTableHTML();
		} catch (Exception $e) {
			print $toPrint;
			errDiv($e -> getMessage());
			return;
		}
		print $toPrint;
	}

	public function deleteItem() {
		global $pdo;
		try {
			$command = "DELETE FROM ".$this -> itemDBE -> tabla." WHERE ".$this -> DBCondition.";";
			$statement = $pdo -> filteredExecute($command, $this -> DBConditionValuesMerged);
			msgDiv($this -> replaceCaption($this -> labels["itemDeletedSuccessfully"]));
		} catch (PDOException $e) {
			errDiv($this -> replaceCaption($this -> labels["itemDeletedError"]).($e -> getMessage()));
		}
	}

	public function setClassInstances() {
		//validateColumnName($this -> itemID);
		$this -> itemDBE = new $this -> itemDBEClass($this -> itemID);
		$this -> DBCondition = ($this -> idField)."=:crudPage_itemID".($this -> additionalDBCondition);
		$this -> DBConditionValuesMerged = array_merge($this -> DBConditionValues, $this -> additionalDBConditionValues);
		$this -> DBConditionValuesMerged["crudPage_itemID"] = $this -> itemID;
		$this -> itemList = new $this -> itemListClass();
		$this -> itemList -> caseImgButtons = $this -> caseImgButtons;
		$this -> itemList -> editItemField = $this -> editItemField;
		$this -> itemList -> deleteItemField = $this -> deleteItemField;
	}

	public function replaceCaption($string) {
		foreach ($this as $k => $e) {
			if (gettype($e) === "string") {
				$string = str_replace("%$k%", $e, $string);
			}
		}
		return $string;
	}
};
?>