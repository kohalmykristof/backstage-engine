<?
class urlap {
	var $formTitle;
	var $formName = "urlap";
	var $mezok_feliratok = array();
	var $mezok_tipusok = array();
	var $values = array();
	var $ssidGet;
	var $width;
	var $tableClass = "";
	var $kotelezo_kitolteni = array();
	var $textInputSize = 65;
	var $numInputSize = 3;
	var $datumDisabled = false;
	var $feliratSzelesseg;
	var $submitName;
	var $submitTitle;
	var $specSubmitText = "";
	var $target = "";
	var $anchor = "";
	var $additional_parameters = array();
	var $check_functions = array(
		"timestamp" => "timestamp_check",
		"dateinput" => "dateinput_check",
		"text" => "text_check",
		"password" => "password_check",
		"textarea" => "textarea_check",
		"num" => "num_check",
		"num_pos" => "num_pos_check",
		"boolRadioButton" => "boolRadioButton_check",
		"check_list" => "check_list_check",
		"imgfile" => "imgfile_check",
		"file" => "file_check",
		"email" => "email_check",
		"select" => "select_check"
	);
	var $kotelezo_csillagozva = false;
	var $ssid = true;
	var $toDb = array();
	var $db_table = "";
	var $mezok_placeholderek = array();
	var $wp = false;
	var $feliratok = array(
		"igen" => "igen",
		"nem" => "nem",
		"email_hibas" => "Megadott e-mail cím hibás!",
		"kotelezo_kitolteni" => "Kötelező kitölteni: %s",
		"url_szures_hibauzenet" => "%s mező nem tartalmazhat linket!"
	);
	var $selectValues = array();
	var $checkListValues = array();
	var $mezok_onchange_jsek = array();
	var $mezok_onprint_jsek = array();
	var $mezok_onkeyup_jsek = array();
	var $gepeloValasztas_include = true;
	var $gepeloValasztas = array();
	var $url_szures = array();
	var $enabled = true;

	function __construct($formTitle, $mezok_feliratok, $mezok_tipusok, $ssidGet, $submitName, $submitTitle) {
		$this -> formTitle = $formTitle;
		$this -> mezok_feliratok = $mezok_feliratok;
		$this -> mezok_tipusok = $mezok_tipusok;
		$this -> ssidGet = $ssidGet;
		$this -> submitName = $submitName;
		$this -> submitTitle = $submitTitle;
		$this -> init();
	}
	function init() {
	
	}
	function fillFromDatabase($command, $values = []) {
		global $pdo;
		try {
			$statement = $pdo -> filteredExecute($command, $values);
			if ($statement -> rowCount() !== 1) {
				return false;
			}
			$result = $statement -> fetch();
			$this -> values = array_merge($this -> values, $result);
			return true;
		} catch (PDOException $e) {
			return false;
		}
	}
	function fillFromPost() {
		if (isset($_POST[$this -> submitName]) && $_POST[$this -> submitName] != "") {
			$this -> values = $_POST;
		}
	}
	function printForm($get_other_string = "", $div = false) {
		if ($this -> gepeloValasztas_include) {
			print "<script src=\"".INTRANET_ADDRESS."/gepeloValasztas.js\"></script>";
		}
		print "<form id=\"".$this -> formName."\" name=\"".$this -> formName."\" enctype=\"multipart/form-data\" action=\"".getUrl($_GET["page"]).$get_other_string.(($this -> anchor !== "") ? ("#".$this -> anchor) : "")."\" method=\"post\"".(($this -> target != "") ? (" target=".$this -> target) : "").">";
		print ($div)
			? "<div id='".$this -> formName."_formTitle' class='formTitle'>"
			: "<center><h3>";
		print $this -> formTitle;
		print ($div)
			? "</div>"
			: "</h3></center><br/>";
		$widthHTML = isset($this -> width)
			?  " width='".$this -> width."'"
			: "";
		$form_header = (($div)
			? "<div id='".$this -> formName."_mezok' class='urlap_mezok'>"
			: "<table".$widthHTML." class='".$this -> tableClass."'>");
		print "<center>$form_header";	
		foreach ($this -> mezok_tipusok as $k => $e) {
			if ($e === "hidden") {
				print "<input name='$k' type='hidden' value='".$this -> values[$k]."'>";
				continue;
			}
			if ($e === "ujtabla") {
				print (($div)
					? "</div>"
					: "</table>");
				print $this -> mezok_feliratok[$k]."$form_header";
				continue;
			}
			$widthHTML = isset($this -> feliratSzelesseg)
				? " width='".$this -> feliratSzelesseg."'"
				: "";
			print ($div)
				? "<div id='".$this -> formName."_$k"."_felirat_mezo' class='urlap_felirat_mezo'><div id='".$this -> formName."_$k"."_felirat"."' class='urlap_felirat'>"
				: "<tr><td".$widthHTML." class='caption' align='right'>";
			print $this -> mezok_feliratok[$k].(($this -> kotelezo_csillagozva && in_array($k, $this -> kotelezo_kitolteni)) ? "<span class='kotelezo_csillag'>*</span>" : "").": ";
			print ($div)
				? "</div><div id='".$this -> formName."_$k"."' class='urlap_mezo'>"
				: "</td><td>";
			$params = array(
				"onchange" => "",
				"onkeyup" => "",
				"autocomplete" => "",
				"placeholder" => "",
				"disabled" => ""
			);
			if (isset($this -> additional_parameters[$e])) {
				$params = $this -> additional_parameters[$e];
			} else if (isset($this -> additional_parameters[$k])) {
				$params = $this -> additional_parameters[$k];
			}

			$params["k"] = $k;
			$params["placeholder"] = isset($this -> mezok_placeholderek[$k]) ? (" placeholder=".(str_replace(" ","&nbsp;",$this -> mezok_placeholderek[$k]))) : "";
			$params["onchange"] = isset($this -> mezok_onchange_jsek[$k]) ? (" onchange=\"".str_replace("\"","&quot;",$this -> mezok_onchange_jsek[$k])."\"") : "";
			$params["onkeyup"] = "";
			if (isset($this -> mezok_onkeyup_jsek[$k])) {
				if ($params["onkeyup"] != "") {
					$params["onkeyup"].= " ";
				}
				$params["onkeyup"].= $this -> mezok_onkeyup_jsek[$k];
			}
			if ($params["onkeyup"] != "") {
				$params["onkeyup"] = " onkeyup=\"".str_replace("\"","&quot;",$params["onkeyup"])."\"";
			}
			$params["disabled"] = "";
			if (!$this -> enabled) {
				$params["disabled"] = " disabled";
			}
			
			$this -> $e($params);
			$gepeloValasztas = false;
			if (isset($this -> gepeloValasztas[$k])) {
				$gV = $this -> gepeloValasztas[$k];
				//if (is_subclass_of($gV, "gepeloValasztas")) {
				$gepeloValasztas = true;
				$gV -> generateJsObject();
				//$params["onkeyup"] = $gV -> jsObject.".printValaszto();";
				//$params["autocomplete"] = " autocomplete='off'";
				//}
			}
			isset($this -> mezok_onprint_jsek[$k]) ? ("<script type='text/javascript'>".$this -> mezok_onprint_jsek[$k]."</script>") : "";
			/* if ($gepeloValasztas) {
				print "<div id='".$gV -> valaszto_div."'></div>";
				print "<script type='text/javascript'>".$gV -> jsObject.".printValaszto();</script>";
			} */
			print ($div)
				? "</div></div>"
				: "</td></tr>";
		}
		print ($div)
			? "</div>"
			: "</table>";
		print "</center><br/>";
		if ($this -> enabled) {	
			print "<center>";
			if ($this -> specSubmitText != "") {
				print $this -> specSubmitText;
			} else {
				print "<input name='".$this -> submitName."' type='submit' value='".$this -> submitTitle."'>";
			}
			print "</center>";
		}
		print "</form><br/>";
	}
	function timestamp(&$params) {
		$k = $params["k"];
		$v = "";
		if ($this -> values[$k] != "") {
			$v = " value='".$this -> values[$k]."'";
		}
		print "<input id='".$this -> formName."_".$k."_input' name='".$k."' value='".$this -> values[$k]."' type='datetime-local'".$v.(($this -> datumDisabled)?" disabled":"").$params["placeholder"].$params["disabled"].">";
	}
	function timestamp_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function dateinput(&$params) {
		$k = $params["k"];
		$v = "";
		if ($this -> values[$k] != "") {
			$v = " value='".$this -> values[$k]."'";
		}
		print "<input id='".$this -> formName."_".$k."_input' name='".$k."' value='".$this -> values[$k]."' type='date'".$v.(($this -> datumDisabled) ? " disabled" : "").$params["placeholder"].$params["disabled"].">";
	}
	function dateinput_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function text(&$params) {
		$k = $params["k"];
		$type = (isset($params["type"])) ? $params["type"] : "text";
		if ($type !== "text") {
			print "<input id='".$this -> formName."_$k"."_input' name='$k' type='$type' value='".(isset($this -> values[$k]) ? $this -> values[$k] : "")."' size='".$this -> textInputSize."'".$params["onchange"].$params["onkeyup"].$params["autocomplete"].$params["placeholder"].$params["disabled"].">";
			return;
		}
		print "<textarea id='".$this -> formName."_$k"."_input' name='$k' rows='1' cols='".$this -> textInputSize."'".$params["onchange"].$params["onkeyup"].$params["autocomplete"].$params["placeholder"].$params["disabled"]." class='textarea_singleline'>".(isset($this -> values[$k]) ? $this -> values[$k] : "")."</textarea>";
		?><script>
			document.getElementById("<? print $this -> formName."_$k"."_input"; ?>").addEventListener("keyup", function() {
				this.value = this.value.replace(/(\r\n|\n|\r)/gm, "");
			});
		</script><?
	}
	function text_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function password(&$params) {
		$params["type"] = "password";
		$this -> text($params);
	}
	function password_check(&$params, &$errorMsg, &$r) {
		$this -> text_check($params, $errorMsg, $r);
	}
	function textarea(&$params) {
		$k = $params["k"];
		print "<textarea id='".$this -> formName."_$k"."_input' name='$k'".$params["onchange"].$params["placeholder"].$params["disabled"].">".$this -> values[$k]."</textarea>";
	}
	function textarea_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function num(&$params) {
		$k = $params["k"];
		print "<input id='".$this -> formName."_$k"."_input' name='$k' type='number' value='".$this -> values[$k]."'".$params["onchange"].$params["placeholder"].$params["disabled"]." size='".$this -> numInputSize."'>";
	}
	function num_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function num_pos(&$params) {
		$k = $params["k"];
		$max = isset($params["max"]) ? (" max='".$params["max"]."'") : "";
		$step = isset($params["step"]) ? (" step='".$params["step"]."'") : "";
		print "<input id='".$this -> formName."_$k"."_input' name='$k' type='number' min='0'$max$step value='".(isset($this -> values[$k]) ? $this -> values[$k] : "")."'".$params["onchange"].$params["placeholder"].$params["disabled"]." size='".$this -> numInputSize."'>";
	}
	function num_pos_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function boolRadioButton(&$params) {
		$k = $params["k"];
		print "<center><input class='".$this -> formName."_$k"."_input' type='radio' name='$k' value='1'".$params["onchange"];
		if (isset($this -> values[$k]) && $this -> values[$k] != "0") {
			print " checked";
		}
		print $params["disabled"]."/> ".$this -> feliratok["igen"]." | <input class='".$this -> formName."_$k"."_input' type='radio' name='$k' value='0'".$params["onchange"];
		if (isset($this -> values[$k]) && $this -> values[$k] == "0") {
			print " checked";
		}
		print $params["disabled"]."/> ".$this -> feliratok["nem"]." </center>";
	}
	function boolRadioButton_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function check_list(&$params) {
		$k = $params["k"];
		if (is_array($this -> checkListValues[$k])) {
			foreach ($this -> checkListValues[$k] as $k2 => $e) {
				//print "<option value='$k2'".(($this -> values[$k]==$k2)?" selected":"").">$e</option>";
				echo "<div style=\"display: inline-block; padding-left: 5px; padding-top: 2px; width: 160px;\"><input type='hidden' name='$k2' value='0' /><input type='checkbox' name='$k2' value='1' ";
				if (isset($this -> values[$k2]) && $this -> values[$k2] == "1") {
					echo "checked";
				}
				echo $params["disabled"]."/><span style=\"position: relative; top: -3px;\"> $e </span></div>";
			}
		}
	}
	function check_list_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function imgfile(&$params) {
		$this -> file($params);
	}
	function imgfile_check(&$params, &$errorMsg, &$r) {
		$this -> file_check($params, $errorMsg, $r);
	}
	function file(&$params) {
		$k = $params["k"];
		print "<input id='".$this -> formName."_$k"."_input' name='$k' type='file'".$params["onchange"].$params["placeholder"].$params["disabled"]." size='".$this -> textInputSize."'>";
	}
	function file_check(&$params, &$errorMsg, &$r) {
		$this -> custom_check($params, $errorMsg, $r);
	}
	function email(&$params) {
		$k = $params["k"];
		$params["type"] = "text";
		$this -> text($params);
		/* print "<input id='".$this -> formName."_$k"."_input' name='$k' type='text' value='".$this -> values[$k]."'".$params["onchange"].$params["placeholder"].$params["disabled"].">"; */
	}
	function email_check(&$params, &$errorMsg, &$r) {
		$r = true;
		$k = $params["k"];
		if (in_array($k,$this -> kotelezo_kitolteni) || $this -> values[$k] != "") {
			if (!filter_var($this -> values[$k],FILTER_VALIDATE_EMAIL)) {
				$r = false;
				$errorMsg = $this -> feliratok["email_hibas"];
				return;
			} else if (!checkdnsrr(substr(strrchr($this -> values[$k], "@"), 1),'MX')) {
				$r = false;
				$errorMsg = $this -> feliratok["email_hibas"];
				return;
			}
		}
		$this -> custom_check($params, $errorMsg, $r);
	}
	function select(&$params) {
		$k = $params["k"];
		print "<select id='".$this -> formName."_$k"."_input' name='$k'".$params["onchange"].$params["disabled"].">";
		if ($this -> mezok_placeholderek[$k] != "") {
			print "<option value='' disabled hidden".(($this -> values[$k]=="") ? " selected" : "").">".$this -> mezok_placeholderek[$k]."</option>";
		}
		if (is_array($this -> selectValues[$k])) {
			foreach ($this -> selectValues[$k] as $k2 => $e) {
				print "<option value='$k2'".(($this -> values[$k] == $k2) ? " selected" : "").">$e</option>";
			}
		}
		print "</select>";
	}
	function select_check(&$params, &$errorMsg, &$r) {
		$r = true;
		$k = $params["k"];
		if ($this -> values[$k] != "" && !isset($this -> selectValues[$k][$this -> values[$k]])) {
			$r = false;
			$errorMsg = "Selected element not found in list: ".$this -> mezok_feliratok[$k];
			return;
		}
		$errorMsg = "Selected element not found in list: ".$this -> mezok_feliratok[$k];
		$this -> custom_check($params, $errorMsg, $r);
	}
	function gepeloValasztas_check(&$params, &$errorMsg, &$r) {
		$r = true;
		$k = $params["k"];
		$gV = $this -> gepeloValasztas[$k];
		if ($this -> values[$k] != "" && $gV -> kotelezo_valasztani && !isset($gV -> ertekek[$this -> values[$gV -> idMezo]])) {
			$r = false;
			$errorMsg = "Selected element not found in list: ".$this -> mezok_feliratok[$k];
			return;
		}
		$this -> custom_check($params, $errorMsg, $r);
	}
	
	function custom_check(&$params, &$errorMsg, &$r) {
		$r = true;
		$k = $params["k"];
		if (in_array($k, $this -> kotelezo_kitolteni)) {
			if ($this -> values[$k] == "") {
				$r = false;
				$errorMsg = str_replace("%s", $this -> mezok_feliratok[$k], $this -> feliratok["kotelezo_kitolteni"]);
			}
		}
	}
	function checkValues(&$errorMsg) {
		foreach ($this -> mezok_tipusok as $k => $e) {
			if (isset($this -> check_functions[$e])) {
				//call_user_func($this -> check_functions[$k],$errorMsg,$r);
				$check_function = $this -> check_functions[$e];
				$params = array("k" => $k);
				$this -> $check_function($params, $errorMsg, $r);
				if (!$r) {
					return false;
				}
			}
			if (isset($this -> gepeloValasztas[$k])) {
				$this -> gepeloValasztas_check($params, $errorMsg, $r);
				if (!$r) {
					return false;
				}
			}
		}
		foreach ($this -> url_szures as $e) {
			if (preg_match('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', $this -> values[$e]) == 1) {
				$errorMsg = str_replace("%s", $this -> mezok_feliratok[$e], $this -> feliratok["url_szures_hibauzenet"]);
				return false;
			}
		}
		return true;
	}
	function addSubmitButton($name, $title, $delimiter, $onclick = "") {
		$this -> specSubmitText.="<input name='$name' type='button' onclick=\"".str_replace("\"", "&quot;", $onclick)."\" value='$title'>$delimiter";
	}
	function handleSubmit(&$errorMsg, $update = false, $condition = "", $conditionValues = []) {
		if (isset($this -> values[$this -> submitName]) && $this -> values[$this -> submitName] != "") {
			if ($this -> checkValues($errorMsg)) {
				if (!$this -> beforeDB($errorMsg)) { return false; }
				if ($update) {
					if (!$this -> updateDB($errorMsg, $condition, $conditionValues)) { return false; }
				} else {
					if (!$this -> insertDB($errorMsg)) { return false; }
				}
				if (!$this -> afterDB($errorMsg)) { return false; }
				return true;
			}
		}
		return false;
	}
	function beforeDB(&$errorMsg) {
		return true;
	}
	function insertDB(&$errorMsg) {
		global $pdo;
		$wpdbValues = [];
		$values = [];
		validateColumnName($this -> db_table);
		$command = "INSERT INTO ".$this -> db_table."(";
		$command2 = "";
		$command3 = "";
		$elso = true;
		validateColumnNames($this -> toDb);
		foreach ($this -> toDb as $k => $e) {
			if ($elso) {
				$elso = false;
			} else {
				$command.= ",";
				$command2.= ",";
				$command3.= ",";
			}
			$command.= $e;
			$command2.= "%s";
			$wpdbValues[] = $this -> values[$e];
			$values[$e] = $this -> values[$e];
			$command3.= ":$e";
		}
		$command.= ") ";
		if ($this -> wp) {
			global $wpdb;
			$wpdb -> form_table = $this -> db_table;
			$command.= "VALUES($command2);";
			if ($wpdb -> query($wpdb -> prepare($command, $wpdbValues)) === false) {
				$errorMsg = $wpdb -> last_error;
				//print $command;
				return false;
			}
			return true;
		} else {
			try {
				$command.= "VALUES($command3);";
				$statement = $pdo -> filteredExecute($command, $values);
				return true;
			} catch (PDOException $e) {
				$errorMsg = $e -> getMessage();;
				return false;
			}
		}
	}
	function updateDB(&$errorMsg, $condition, $conditionValues = []) {
		global $pdo;
		$wpdbValues = [];
		$values = [];
		validateColumnName($this -> db_table);
		$command = "UPDATE ".$this -> db_table." SET ";
		$command2 = "";
		$command3 = "";
		$elso = true;
		validateColumnNames($this -> toDb);
		foreach ($this -> toDb as $k => $e) {
			if ($elso) {
				$elso = false;
			} else {
				$command2.= ",";
				$command3.= ",";
			}
			$command2.= "$e='%s'";
			$wpdbValues[] = $this -> values[$e];
			$values["value_".$e] = $this -> values[$e];
			$command3.= "$e=:value_$e";
		}
		$wpdbValues = array_merge($wpdbValues, $conditionValues);
		$values = array_merge($values, $conditionValues);
		if ($this -> wp) {
			global $wpdb;
			$wpdb -> form_table = $this -> db_table;
			$command.= $command2." WHERE $condition;";
			if ($wpdb -> query($wpdb -> prepare($command, $wpdbValues)) === false) {
				$errorMsg = $wpdb -> last_error;
				return false;
			}
		} else {
			try {
				$command.= $command3." WHERE $condition;";
				$statement = $pdo -> filteredExecute($command, $values);
				return true;
			} catch (PDOException $e) {
				$errorMsg = $e -> getMessage();
				return false;
			}
		}
	}
	function afterDB(&$errorMsg) {
		foreach ($_POST as $k => $e) {
			unset($_POST[$k]);
		}
		foreach ($this -> values as $k => $e) {
			unset($this -> values[$k]);
		}
		return true;
	}
}

class gepeloValasztas {
	var $urlap;
	var $k;
	var $valaszto;
	var $valaszto_div;
	var $idMezo;

	var $szavankent = false;
	var $lista = false;
	var $init = true;
	var $ertekek = array();
	var $dropDownLength = 8;
	var $jsObject;
	var $kotelezo_valasztani = true;

	function gepeloValasztas($urlap, $k, $valaszto, $valaszto_div, $ertekek = array(), $idMezo = "") {
		$this -> urlap = $urlap;
		$this -> k = $k;
		$this -> valaszto = $valaszto;
		$this -> valaszto_div = $valaszto_div;
		$this -> idMezo = $idMezo;
		$this -> ertekek = $ertekek;
	}
	function generateJsObject() {
		$gV_nev = $this -> urlap."_".$this -> k."_gv";
		$this -> valaszto_div = $this -> urlap."_".$this -> valaszto_div;
		$gVConfigVar = $this -> urlap."_".$this -> k."_gv_config";
		?>
		<script type="text/javascript">
			var <? print $gVConfigVar; ?> = {
				urlap: "<? print $this -> urlap; ?>",
				k: "<? print $this -> k; ?>",
				valaszto: "<? print $this -> valaszto; ?>",
				valaszto_div: "<? print $this -> valaszto_div; ?>",
				idMezo: "<? print $this -> idMezo; ?>",
				szavankent: <? print ($this -> szavankent)?"true":"false"; ?>,
				lista: <? print ($this -> lista)?"true":"false"; ?>,
				init: <? print ($this -> init)?"true":"false"; ?>,
				dropDownLength: <? print $this -> dropDownLength; ?>,
				jsObject: '<? print $gV_nev; ?>'
			};
			var <? print $gV_nev; ?> = createGepeloValasztas(<? print $gVConfigVar; ?>);
			<? print $gV_nev; ?>.setValues(<? print json_encode($this -> ertekek); ?>);
		</script><?
		$this -> jsObject = $gV_nev;
	}
	function fromDB($tabla, $oszlop_k, $oszlop_e, $orderBy = "") {
		global $pdo;

		if (strpos($orderBy, ";") !== false) {
			throw new Error("orderBy parameter must not contain semicolon characters!");
		}
		validateColumnNames([$tabla, $oszlop_k, $oszlop_e]);

		$orderBy = ($orderBy  !== "") ? (" ORDER BY ".$orderBy) : "";
		try {
			$command = "SELECT $oszlop_k as k, $oszlop_e as e FROM $tabla".$orderBy.";";
			$statement = $pdo -> filteredExecute($command, []);
			while ($p = $statement -> fetch()) {
				$this -> ertekek[$p["k"]] = $p["e"];
			}
			return true;
		} catch (PDOException $e) {
			return false;
		}
	}
}
?>