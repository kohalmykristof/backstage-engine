<?
class DBE {
	var $mezok;
	var $feliratok;
	var $tabla;
	var $urlap;
	var $values = array();
	var $update = false;
	var $id;
	public function __construct($id = 0) {
		if ($id !== 0) {
			$this -> update = true;
		}
		$this -> id = $id;
	}

	public function load($selector, $queryValues = []) {
		if (!$values = $this -> loadRow($this -> tabla, $selector, $queryValues)) {
			return false;
		}
		$this -> values = $values;
		$this -> urlap -> values = $values;
		return true;
	}

	public function loadRow($table, $selector, $queryValues = []) {
		global $pdo;
		if (!is_array($queryValues)) { $queryValues = []; }

		try {
			$command = "SELECT * FROM $table WHERE $selector;";
			$statement = $pdo -> filteredExecute($command, $queryValues);
			if ($statement -> rowCount() !== 1) {
				return false;
			}
			$result = $statement -> fetch();
			return $result;
		} catch (PDOException $e) {
			return false;
		}
	}

	public function handleSubmit(&$errorMsg, $condition = "", $conditionValues = []) {
		$this -> urlap -> fillFromPost();
		return $this -> urlap -> handleSubmit($errorMsg, $this -> update, $condition, $conditionValues);
	}

	/*
	public function add() {
		
	}
	
	public function update() {
		
	}
	*/

	public function printUrlap($get_other_string = "") {
		$this -> urlap -> printForm($get_other_string);
	}
	
	public function datum($datum) {
		return str_replace("-",".",$datum);
	}

	public function sortStringLength($a, $b) {
		$ekezetek = array("á","é","í","ó","ö","ő","ú","ü","ű","Á","É","Í","Ó","Ö","Ő","Ú","Ü","Ű");
		$csere = array("a","e","i","o","o","o","u","u","u","a","e","i","o","o","o","u","u","u");
		return strlen(str_replace($ekezetek, $csere, strtolower($b))) - strlen(str_replace($ekezetek, $csere, strtolower($a)));
	}
}
?>