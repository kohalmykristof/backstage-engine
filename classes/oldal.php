<?
class oldal {
	var $cim;
	var $menu;
	var $dropDownGroup;
	var $dropDownCaption;
	var $kepessegek = array();
	var $styles = array();
	var $javascripts = array();
	public function __construct($cim, $menu, $kepessegek, $dropDownGroup = "", $dropDownCaption = "") {
		$this -> cim = $cim;
		$this -> menu = $menu;
		$this -> kepessegek = $kepessegek;
		$this -> dropDownGroup = $dropDownGroup;
		$this -> dropDownCaption = $dropDownCaption;
	}
	function addDropzone($min = true, $css = true) {
		if ($min) {
			$this -> javascripts[] = INTRANET_ADDRESS."/lib/dropzone/dist/min/dropzone.min.js";
		} else {
			$this -> javascripts[] = INTRANET_ADDRESS."/lib/dropzone/dist/dropzone.js";
		}
		if (!$css) {
			return;
		}
		if ($min) {
			$this -> styles[] = INTRANET_ADDRESS."/lib/dropzone/dist/min/dropzone.min.css";
		} else {
			$this -> styles[] = INTRANET_ADDRESS."/lib/dropzone/dist/dropzone.css";
		}
	}
	function addTypingSearch() {
		$this -> javascripts[] = INTRANET_ADDRESS."/gepeloValasztas.js";
	}
}
?>