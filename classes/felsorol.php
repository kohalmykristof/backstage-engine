<?
class felsorolas {
	var $id = "";
	var $values=array();
	var $keyColumn;
	var $mainTable;
	//var $elemek_kornyezet=array();
	var $mezok_megjeleniteni=array();
	var $computeFunctions=array();
	var $feliratok;
	var $tabla_fejlec_feliratok=array();
	var $table_width=500;
	var $control_feliratok=array();
	var $control_linkek=array();
	var $control_blank=array();
	var $control_size=7;
	var $controlsInColumn=false;
	var $control_ColumnDelimiter="<br/>";
	var $imgControlTrue=array();
	var $imgControlFalse=array();
	var $caseImgButtons = array();
	var $ssidGet;
	var $orderList_include = true;
	var $orderBy = array();
	var $filterList_include = true;
	var $filterBy = array();
	var $filterCaptions = array();
	var $editItemField = "editItem";
	var $deleteItemField = "deleteItem";
	var $centerWrapperTagEnabled = true;

	public function __construct($config) {
		if (!is_array($config)) {
			throw new Exception("config is mandatory and it has to be an associative array!");
		}
		if (!isset($config["keyColumn"])) {
			throw new Exception("config[keyColumn] is mandatory!");
		}
		/* if (!isset($config["mainTable"])) {
			throw new Exception("config[mainTable] is mandatory!");
		} */

		$this -> id = $config["id"];
		$this -> keyColumn = $config["keyColumn"];
		$this -> mainTable = $config["mainTable"];
		if (isset($config["editItemField"])) {
			$this -> editItemField = $config["editItemField"];
		}
		if (isset($config["deleteItemField"])) {
			$this -> deleteItemField = $config["deleteItemField"];
		}
	}

	public function addColumn($columnName, $column) {
		if (!is_array($column)) {
			return;
		}
		$this -> mezok_megjeleniteni[] = $columnName;
		$this -> tabla_fejlec_feliratok[] = $column["label"];
		if (isset($column["orderBy"]) && $column["orderBy"]) {
			$this -> orderBy[] = $columnName;
		}
		if (isset($column["filterBy"]) && is_array($column["filterBy"])) {
			$this -> filterBy[$columnName] = $column["filterBy"];
			$this -> filterCaptions[$columnName] = $column["filterCaption"];
		}
	}

	public function addColumns($columns) {
		if (!is_array($columns)) {
			return;
		}
		foreach ($columns as $k => $e) {
			$this -> addColumn($k, $e);
		}
	}

	public function setColumns($columns) {
		if (!is_array($columns)) {
			return;
		}
		$this -> deleteColumns();
		$this -> addColumns($columns);
	}

	public function deleteColumns() {
		$this -> mezok_megjeleniteni = array();
		$this -> tabla_fejlec_feliratok = array();
		$this -> orderBy = array();
		$this -> filterBy = array();
		$this -> filterCaptions = array();
	}
	
	public function fillFromDatabase($command = "", $values = []) {
		global $pdo;
		if ($command === "") {
			$command = "SELECT * FROM ".$this -> mainTable.";";
		}
		try {
			$statement = $pdo -> filteredExecute($command, $values);
			if ($statement -> rowCount() === 0) {
				return false;
			}
			while ($v = $statement -> fetch()) {
				$this -> values[$v[$this -> keyColumn]] = $v;
			}
		} catch (PDOException $e) {
			throw new Exception(($e -> getMessage())."<br/>Command: ".$command);
		}
		return true;
	}
	public function joinFieldsFromDatabase($config) {

		global $pdo;
		
		if (!is_array($config)) {
			throw new Exception("config is mandatory and it has to be an array!");
		}
		if (!isset($config["fieldsInDB"]) || !is_array($config["fieldsInDB"])) {
			throw new Exception("config[fieldsInDB] is mandatory and it has to be an array!");
		}
		if (!isset($config["tableSecond"])) {
			throw new Exception("config[tableSecond] is mandatory!");
		}
		validateColumnNames($config["fieldsInDB"]);
		if (isset($config["fieldsInClass"]) && is_array($config["fieldsInClass"])) {
			validateColumnNames($config["fieldsInClass"]);
		}
		validateColumnName($config["tableSecond"]);
		$defaultConfig = [
			"fieldsInClass" => array(),
			"tableMain" => $this -> mainTable,
			"joinFieldMain" => $this -> keyColumn,
			"keyColumn" => $this -> keyColumn,
			"selectorQuery" => "",
			"selectorValues" => array(),
			"itemCallback" => ""
		];
		$config = array_merge($defaultConfig, $config);
		validateColumnName($config["tableMain"]);
		validateColumnName($config["joinFieldMain"]);
		validateColumnName($config["keyColumn"]);

		$fieldsInDB = $config["fieldsInDB"];
		$fieldsInClass = $config["fieldsInClass"];
		$tableMain = $config["tableMain"];
		$joinFieldMain = $config["joinFieldMain"];
		$tableSecond = $config["tableSecond"];
		$joinFieldSecond = isset($config["joinFieldSecond"]) ? $config["joinFieldSecond"] : $joinFieldMain;
		validateColumnName($joinFieldSecond);
		$keyTable = isset($config["keyTable"]) ? $config["keyTable"] : $tableMain;
		validateColumnName($keyTable);
		$keyColumn = $config["keyColumn"];
		$selectorQuery = $config["selectorQuery"];
		$selectorValues = $config["selectorValues"];
		$itemCallback = $config["itemCallback"];

		if ($selectorQuery !== "") {
			$selectorQuery = " AND $selectorQuery";
		}
		$elso = true;
		$fields = "";
		for ($i = 0; $i < count($fieldsInDB); $i++) { 
			if ($elso) {
				$elso = false;
			} else {
				$fields.= ", ";
			}
			$fields.= $fieldsInDB[$i];
			if (isset($fieldsInClass[$i])) {
				$fields.=" as ".$fieldsInClass[$i];
			}
		}
		try {
			$command = "SELECT $fields, $keyTable.".$keyColumn." as $keyColumn FROM $tableMain, $tableSecond WHERE $tableMain.$joinFieldMain = $tableSecond.$joinFieldSecond".$selectorQuery.";";
			$statement = $pdo -> filteredExecute($command, $selectorValues);
			while ($v = $statement -> fetch()) {
				$key = $v[$keyColumn];
				if (isset($this -> values[$key])) {
					foreach ($v as $k => $e) {
						if ($k === $key) {
							continue;
						}
						$this -> values[$key][$k]=$e;
					}
					if ($itemCallback !== "" && is_callable($itemCallback)) {
						$this -> values[$key]=$itemCallback($this -> values[$key]);
					}
				}
			}
		} catch (PDOException $e) {
			throw new Exception(($e -> getMessage)."<br/>Command: ".$command);
		}
		return true;
	}
	public function printList() {
		$toPrint="";
		foreach ($this -> values as $k => $e) {
			//print $e;
			/* if (!isset($this -> elemek_kornyezet[0])) {
				$this -> elemek_kornyezet[0]="%elem%";
			} */
			//$toPrint.= str_replace($this -> elemek_kornyezet[0],'%elem%',$e[$this -> mezok_megjeleniteni[0]]);
			$toPrint.= "<div class='list_".$this -> mezok_megjeleniteni[0]."'>".$e[$this -> mezok_megjeleniteni[0]]."</div>";
			$toPrint.= "<br/>";
		}
		return $toPrint;
	}
	public function generateTableHTML() {
		$toPrint="";
		if ($this -> orderList_include) {
			print "<script src=\"".INTRANET_ADDRESS."/orderList.js\"></script>";
		}
		if ($this -> filterList_include) {
			print "<script src=\"".INTRANET_ADDRESS."/filterList.js\"></script>";
		}
		if (count($this -> filterBy) > 0) {
			$toPrint.= "<div id = 'table_filter_".$this -> id."' class='table_filter_parent'></div>";
			$elso = true;
			$filterScript = "
<script>
(function() {
	var parentElement = document.getElementById('table_filter_".$this -> id."');
	var filters = {";
			foreach ($this -> filterBy as $k => $e) {
				$filterScript.= $elso?"":", "; $elso=false;
				$filterScript.="
		'$k': {
			tdClass: 'td_$k',
			title: '".(($this -> filterCaptions[$k])?:"")."',
			chkValues: [";
				$elso2 = true;
				for ($i = 0; $i < count($e); $i++) {
					$filterScript.= $elso2?"":", ";
					$elso2 = false;
					$filterScript.="
				{
					value: '".$e[$i]."',
					caption: '".$e[$i]."'
				}";
				}
				$filterScript.="
			]
		}";
			}
			$filterScript.="
	};
	var filterConfig = {
		filters: filters,
		parentElement: parentElement,
		tableId: 'table_".$this -> id."',
		exactMatch: false
	};
	createFilterList(filterConfig);
})();
</script>";
			$toPrint.= $filterScript;
		}
		$toPrint.= ($this -> centerWrapperTagEnabled) ? "<center>" : "";
		$toPrint.= "<table id='table_".$this -> id."' class='table_".$this -> id."' width='".$this -> table_width."'><tr>";
		$i=0;
		foreach ($this -> tabla_fejlec_feliratok as $k => $e) {
			$orderScript = "";
			if ($i<sizeof($this -> mezok_megjeleniteni)) {
				$orderScript = "";
				if (in_array($this -> mezok_megjeleniteni[$k], $this -> orderBy)) {
					$ascButtonId = $this -> id."_order_asc_".$k;
					$descButtonId = $this -> id."_order_desc_".$k;
					$orderScript = "
<div class='up_down_button'>
	<button id='".$ascButtonId."' class='order_asc'>
		<svg viewBox='0 0 20 24'>
			<use xlink:href='".INTRANET_ADDRESS."/images/up_down_button.svg#a'></use>
		</svg></button>
	<script>
		createOrderList({
			tableId: 'table_".$this -> id."',
			tdClass: 'td_".$this -> mezok_megjeleniteni[$k]."',
			desc: false,
			buttonId: '".$ascButtonId."'
		});
	</script>
	<button id='".$descButtonId."' class='order_desc'>
		<svg viewBox='0 0 20 24'>
			<use xlink:href='".INTRANET_ADDRESS."/images/up_down_button.svg#b'></use>
		</svg></button>
	<script>
		createOrderList({
			tableId: 'table_".$this -> id."',
			tdClass: 'td_".$this -> mezok_megjeleniteni[$k]."',
			desc: true,
			buttonId: '".$descButtonId."'
		});
	</script>
</div>";
				}
				$toPrint.= "<th class='th_".$this -> mezok_megjeleniteni[$k]."'>$e".$orderScript."</th>";
			} else {
				$controlHeader[] = $e;
			}
			$i++;
		}
		if ($this -> controlsInColumn) {
			$toPrint.= "<th class='th_control'>".$controlHeader[0]."</th>";
		} else {
			$i=0;
			foreach ($this -> control_feliratok as $k => $e) {
				$toPrint.= "<th class='th_control'>".$controlHeader[$i]."</th>";
				$i++;
			}
		}
		foreach ($this -> imgControlTrue as $k => $e) {
			$toPrint.="<th class='th_control'>&nbsp;</th>";
		}
		foreach ($this -> caseImgButtons as $k => $e) {
			$toPrint.="<th class='th_control'>&nbsp;</th>";
		}
		$toPrint.= "</tr>";
		//generate computed values, replace empty values spaces:
		foreach ($this -> values as $k => $e) {
			foreach ($this -> mezok_megjeleniteni as $m) {
				$value = isset($e[$m]) ? $e[$m] : "";
				if (isset($this -> computeFunctions[$m]) && is_callable($this -> computeFunctions[$m])) {
					$value = $this -> computeFunctions[$m]($e);
				}
				if ($value === "") {
					$value = "&nbsp;&nbsp;";
				}
				$this -> values[$k][$m] = $value;
			}
		}

		foreach ($this -> values as $k => $e) {
			$toPrint.= "<tr>";
			foreach ($this -> mezok_megjeleniteni as $m) {
				$value = $e[$m];
				
				/* if (isset($this -> elemek_kornyezet[$m])) {
					$value = str_replace("%elem%",$value,$this -> elemek_kornyezet[$m]);
				} */
				$toPrint.= "<td align=\"center\" class=\"td_".$m."\">$value</td>";
				//print "<td>".$this -> elemek_kornyezet[$m]."</td>";
			}
			if ($this -> controlsInColumn) {
				$toPrint.= "<td align=\"center\">";
			}
			foreach ($this -> control_linkek as $kl => $el) {
				$link=str_replace("%SSID%", "session_id=".$this -> ssidGet, $el);
				$link=str_replace("%id%", "id=".$k, $link);
				foreach ($this -> values[$k] as $field => $value) {
					if (settype($value, "string")) {
						$link = str_replace("%$field%", $value, $link);
					}
				}
				if (!$this -> controlsInColumn) { $toPrint.= "<td align=\"center\">"; }
				$toPrint.= "<div style=\"\"><form action=\"$link\" method=\"post\" target=\"".$this -> control_blank[$kl]."\"><input name=\"$kl\" type=\"submit\" value=\"".$this -> feliratok[$this -> control_feliratok[$kl]]."\" style=\"width: ".$this -> control_size."em; height: 2em;\"></form></div>";
				if ($this -> controlsInColumn) {
					$toPrint.= $this -> control_ColumnDelimiter;
				} else { $toPrint.= "</td>"; }
			}
			if ($this -> controlsInColumn) {
				$toPrint.= "</td>";
			}
			if (is_array($this -> imgControlTrue)) {	
				foreach ($this -> imgControlTrue as $k2 => $e2) {
					if ($e[$k2]=='1') {
						$imgControl = $e2;
					} else {
						$imgControl = $this -> imgControlFalse[$k2];
					}
					$imgControl = str_replace("%id%", $k, $imgControl);
					foreach ($this -> values[$k] as $field => $value) {
						if (settype($value, "string")) {
							$imgControl = str_replace("%$field%", $value, $imgControl);
						}
					}
					$toPrint.= "<td class='td_control'>".$imgControl."</td>";
				}
			}
			if (is_array($this -> caseImgButtons)) {
				foreach ($this -> caseImgButtons as $k2 => $e2) {
					if (!is_array($e2)) {
						continue;
					}
					$default = false;
					if (!isset($e2["valueField"]) || $e2["valueField"] === "") {
						$default = true;
					} else if (!isset($e[$e2["valueField"]]) || $e[$e2["valueField"]] === "") {
						$default = true;
					}
					$value = "";
					if (!$default) { $value = $e[$e2["valueField"]]; }
					if (!$default && (!isset($e2["cases"]) || !is_array($e2["cases"]))) {
						$default = true;
					}
					if (!$default && (!isset($e2["cases"][$value]) || !is_array($e2["cases"][$value]))) {
						$default = true;
					}
					$caseConfig = "";
					if ($default) {
						if (!isset($e2["default"]) || !is_array($e2["default"])) {
							continue;
						}
						$caseConfig = $e2["default"];
					} else {
						$caseConfig = $e2["cases"][$value];
					}
					if (!isset($caseConfig["controlTemplate"]) && !isset($caseConfig["img"])) {
						continue;
					}
					$caseImgButton = "";
					if (isset($caseConfig["controlTemplate"])) {
						$caseImgButton = $caseConfig["controlTemplate"];
					} else {
						$form = " form=\"".$k2."_%id%\"";
						$onclickScript = "document.getElementById('".$k2."_%id%').submit();";
						if (isset($caseConfig["confirmQuestion"]) && $caseConfig["confirmQuestion"] !== "") {
							$confirmQuestion = str_replace("'", "\'", $caseConfig["confirmQuestion"]);
							$form = "";
							$onclickScript = "confirmSubmitForm('".$k2."_%id%','$confirmQuestion','imgControlConfirmed','true'); return false;";
						}
						$caseImgButton = "<form action=\"".getUrl($_GET["page"])."\" id=\"".$k2."_%id%\" name=\"".$k2."_%id%\" method=\"POST\"><input name=\"".$this -> id."\" type=\"hidden\" value=\"%id%\" form=\"".$k2."_%id%\"><input name=\"".$k2."\" type=\"hidden\" value=\"true\"><input name=\"".$k2."_imageControl\" type=\"image\" src=\"".$caseConfig["img"]."\" height=\"30\" width=\"30\"$form onclick=\"$onclickScript\" title=\"".$caseConfig["title"]."\"></form>";
					}
					$caseImgButton = str_replace("%id%", $k, $caseImgButton);
					foreach ($this -> values[$k] as $field => $value) {
						if (settype($value, "string")) {
							$caseImgButton = str_replace("%$field%", $value, $caseImgButton);
						}
					}
					$toPrint.= "<td class='td_control'>".$caseImgButton."</td>";
				}
			}
			$toPrint.= "</tr>";
		}
		$toPrint.= "</table>";
		$toPrint.= ($this -> centerWrapperTagEnabled) ? "</center>" : "";
		return $toPrint;
	}
}

?>