<?
class PDOe extends PDO {
	public function __construct() {
		$args = func_get_args();
		call_user_func_array(array($this, 'parent::__construct'), $args);
	}
	public function filteredExecute($query, $params) {
		$filteredParams = [];
		foreach ($params as $k => $e) {
			$keyword = ":".(($k[0] === ":") ? substr($k, 1) : $k);
			if (strpos($query, $keyword) !== false) {
				$filteredParams[$keyword] = $e;
			}
		}
		//var_dump($filteredParams);
		$statement = parent::prepare($query);
		$statement -> execute($filteredParams);
		return $statement;
	}
}
?>