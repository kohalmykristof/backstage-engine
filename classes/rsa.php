<?
class rsaEncryption {
	public $pubkey = "";
	public $privkey = "";

	public function encrypt($sourceData) {
		$keyLength = 0;
		$pkey = openssl_get_publickey($this -> pubkey);
		$keyData = openssl_pkey_get_details($pkey);
		$keyLength = $keyData["bits"];
		$maxLength = ($keyLength / 8) - 42;
		$encryptedData = "";
		while ($sourceData) {
			$sourceDataPart = substr($sourceData, 0, $maxLength);
			$sourceData = substr($sourceData, $maxLength);
			if (openssl_public_encrypt($sourceDataPart, $encryptedDataPart, $this -> pubkey)) {
				$encryptedData.= $encryptedDataPart;
			} else {
				throw new Exception("Unable to encrypt data. Perhaps it is bigger than the key size?");
			}
		}
		return base64_encode($encryptedData);
	}

	public function decrypt($encryptedData) {
		$keyLength = 0;
		$pkey = openssl_get_privatekey($this -> privkey);
		$keyData = openssl_pkey_get_details($pkey);
		$keyLength = $keyData["bits"];
		$maxLength = $keyLength / 8;
		$encryptedData = base64_decode($encryptedData);
		$decryptedData = "";
		while ($encryptedData) {
			$encryptedDataPart = substr($encryptedData, 0, $maxLength);
			$encryptedData = substr($encryptedData, $maxLength);
			if (openssl_private_decrypt($encryptedDataPart, $decryptedDataPart, $this -> privkey)) {
				$decryptedData.= $decryptedDataPart;
			} else {
				// $data = '';
			}
		}
		return $decryptedData;
	}
}
?>