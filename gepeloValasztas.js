function createGepeloValasztas(config){

	var urlap = config.urlap;
	var k = config.k;
	var valaszto = config.valaszto;
	var valaszto_div_id = config.valaszto_div;
	var idMezo = config.idMezo || (config.k + "_id");

	var szavankent = config.szavankent || false;
	var lista = config.lista || false;
	var init = config.init || true;
	var dropDownLength = config.dropDownLenght || 8;
	var jsObject = config.jsObject || "";
	var callback = config.callback || function(){};
	
	var valasztott_ertek = "";
	var valasztott_id = "";
	var ertekek = [];

	var idInput = document.forms[urlap][idMezo];
	if (!idInput) {
		idInput = document.createElement('input');
		idInput.type = 'hidden';
		idInput.name = idMezo;
		idInput.value = "";
		document.forms[urlap].appendChild(idInput);
	}

	var gepeltInput = document.forms[urlap][k];
	if (!gepeltInput) {
		throw new Error("Input '" + k + "' cannot be found in form '" + urlap + "'!");
	}

	var selectDiv = document.getElementById(valaszto_div_id);
	if (!selectDiv) {
		selectDiv = document.createElement("div");
		selectDiv.setAttribute("id", valaszto_div_id);
		gepeltInput.parentNode.appendChild(selectDiv);
	}

	var valaszt = function(){
		var s=document.getElementById(urlap+"_"+valaszto);
		valasztott_id=s.value;
		valasztott_ertek=s.options[s.selectedIndex].text;
		document.forms[urlap][k].value=valasztott_ertek;
		idInput.value = valasztott_id;
		selectDiv.innerHTML="";
		callback();
	};

	var printValaszto = function(){
		//alert("zs");
		var gepeltErtek=gepeltInput.value;
		var toPrint="<select id=\""+urlap+"_"+valaszto+"\" name=\""+valaszto+"\" form=\""+urlap+"\" onclick='"+jsObject+".valaszt();'>";
		var toPrint_lista="";
		var id;
		var ures=true;
		var egyezes=false;
		var egyezesek_szama=0;
		var egyezes_id="";
		var egyezes_ertek="";
		var egyezes_index="";
		var i=0;
		var k2;
		/* var vDiv=document[urlap][k+'_id'];
		if(vDiv){
			vDiv.value="";
		} */
		idInput.value = "";
		for(k2 in ertekek){
			var e2=ertekek[k2];
			id=k2;
			//if(typeof(e2)=='string'){
			if(gepeltErtek.length<=e2.length){
				//var e2_ = e2.substring(0,gepeltErtek.length);
				//if(gepeltErtek.toLowerCase()==e2_.toLowerCase()){
				var talalat=false;
				if(szavankent){
					var szavak = gepeltErtek.toLowerCase().split(" ");
					talalat=true;
					for(i in szavak){
						//alert("zs");
					
						if(e2.toLowerCase().indexOf(szavak[i])==-1){
							talalat=false;
							break;
						}
					}
				} else {
					if(e2.toLowerCase().indexOf(gepeltErtek.toLowerCase())>-1){
						talalat=true;
					}
				}
				if(talalat){
					toPrint+="";
					toPrint+="<option value=\""+id+"\">"+e2+"</option>";
					if(!ures){
						toPrint_lista+=", ";
					}
					toPrint_lista+=e2;
					ures=false;
					if(gepeltErtek.toLowerCase()==e2.toLowerCase()){
						egyezes=true;
						egyezesek_szama++;
						egyezes_id=id;
						egyezes_ertek=e2;
						egyezes_index=i;
					}
					i++;
				}
				//}
			}
			//} else {
			//	id=e2;
			//}
			
		};
		toPrint+="<option value=\"\" selected disabled style=\"display: none;\"></option></select>";
		if(lista){
			selectDiv.innerHTML=toPrint_lista;
		} else {
			selectDiv.innerHTML=toPrint;
		}
		if(ures || gepeltErtek.length==0){
			selectDiv.innerHTML="";
		} else if(!ures && egyezesek_szama==1 && init){
			//selectDiv.innerHTML="";
			gepeltInput.value=egyezes_ertek;
			/*if(vDiv){
				vDiv.value=egyezes_id;
			}*/
			//alert(valasztFuggveny.split("&quot;").join("\"")+";");
			document.getElementById(urlap+"_"+valaszto).selectedIndex=egyezes_index;
			valaszt();
			//eval(valasztFuggveny.split("&quot;").join("\"")+";");
		} else {
			if(!lista){
				var dropdownselect = document.getElementById(urlap+"_"+valaszto);
				var length = dropdownselect.length;
				if(length>dropDownLength){
					length=dropDownLength;
				}
				if(length==1){
					length=2;
				}
				dropdownselect.size = length;
			}
		}
	};

	var ertekek_JSON = function(ertekekJSON){
		ertekek = JSON.parse(ertekekJSON);
		printValaszto();
	};

	var setValues = function(newValues) {
		if (typeof newValues !== "object") {
			throw new Error("newValues has to be an object!");
		}
		ertekek = newValues;
		printValaszto();
	}

	gepeltInput.addEventListener("keyup", function() {
		printValaszto();
	});
	gepeltInput.setAttribute("autocomplete", "off");

	return {
		valaszt: valaszt,
		printValaszto: printValaszto,
		ertekek_JSON: ertekek_JSON,
		setValues: setValues
	};
}