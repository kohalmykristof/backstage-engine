<?
$feliratok["login"] = array("Login");
$feliratok["login_ujra"] = array("Log in again");
$feliratok["login_adatbazis_hiba"] = array("Login failed due to database error!");
$feliratok["hibas_login"] = array("Incorrect username and/or password!");
$feliratok["hibas_session_azonositas"] = array("Invalid session id and/or session cookie expired! Please log in again!");
$feliratok["logout"] = array("Logout");
$feliratok["logged_in_as"] = array("Logged in as");
$feliratok["nincs_jogosultsag"] = array("You don't have permission to access this page!");
$feliratok["login_felhasznalonev"] = array("Username");
$feliratok["login_jelszo"] = array("Password");
$feliratok["adatbazis_kapcsolat_hiba"] = array("Connecting to MySQL server failed!");
$feliratok["db_auth_error"] = array("Database authentication error.");
$feliratok["adatbazis_nem_talalhato"] = array("Cannot find database.");
$feliratok["adatbazis_hiba"] = array("Database error!");
$feliratok["oldal_nem_talalhato"] = array("Error 404 - requested page not found!");
$feliratok["email_hibas"] = array("Given e-mail address is invalid! You have to give an e-mail with the correct e-mail address format and with a valid e-mail domain!");
$feliratok["pw_format_min_length_error"] = array("Password must be at least %NUM% characters length!");
$feliratok["pw_format_min_numbers_error"] = array("Password must contain at least %NUM% numeric character(s)!");
$feliratok["pw_format_min_letters_error"] = array("Password must contain at least %NUM% letter character(s)!");
$feliratok["pw_format_min_nonalphanumerics_error"] = array("Password must contain at least %NUM% non-alphanumeric character(s)!");
$feliratok["pw_format_injection_chars_error"] = array("Password cannot contain backslash (\\), apostrophe (') or quote (\") characters!");
?>