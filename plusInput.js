function createPlusInput(config){
	var id = config.id;
	var createElementFunction = config.createElementFunction;
	var valuesJSON = config.valuesJSON;
	var parentDiv = config.parentDiv;
	var formName = config.formName;
	var addControlClass = config.addControlClass;
	var addControlInnerHTML = config.addControlInnerHTML;
	var values = JSON.parse(valuesJSON);
	var elements = [];
	var self = this;
	var maxindex=0;
	var enabled = true;
	var addControl;
	init();
	
	function init(){
		values.forEach(function(value,index,array){
			add(index,value);
			if(index>maxindex){
				maxindex=index;
			}
		});
		maxindex++;
		add(maxindex,'');
		addControl=document.createElement("a");
		addControl.setAttribute("class",addControlClass);
		addControl.innerHTML=addControlInnerHTML;
		addControl.onclick=function(){
			addnew();
		};
		parentDiv.appendChild(addControl);
	}
	
	function add(index,value){
		console.log(index,value);
		values[index]=value;
		elements[index]=createElementFunction(self.add,self.remove,index,value,formName);
		parentDiv.appendChild(elements[index]);
	}
	function addnew(){
		if (!enabled) {
			return;
		}
		maxindex++;
		add(maxindex,'');
	}
	function remove(index){
		parentDiv.removeChild(elements[index]);
		elements.slice(index,1);
	}
	function enable(){
		addControl.style.display = "block";
		enabled = true;
	}
	function disable(){
		addControl.style.display = "none";
		enabled = false;
	}
	var obj = {
		add: add,
		remove: remove,
		addnew: addnew,
		enable: enable,
		disable: disable
	};
	return obj;
}