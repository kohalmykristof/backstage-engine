function confirmSubmitForm(form, question, key, value) {
	var r = confirm(question);
	if (r === true) {
		document.getElementById(form).elements[key].value=value;
		document.getElementById(form).submit();
	}
}