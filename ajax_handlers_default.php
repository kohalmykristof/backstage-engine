<?
	$ajaxHandlers = array(
		"getusersalt" => array(
			"handler" => dirname(__FILE__)."/auth/getUserSaltAjax.php",
			"capabilities" => array()
		),
		"getIsLDAPuser" => array(
			"handler" => dirname(__FILE__)."/auth/getIsLDAPUserAjax.php",
			"capabilities" => array()
		)
	);
?>