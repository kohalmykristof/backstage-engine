# Change Log

## [1.0.2] - 2019-01-10
### Added
- emailHtmlToText function for generating text versions from HTML email messages
- LDAP_PASSWORD_CHARACTERS_WHITELIST defined constant for arbitrary LDAP password character whitelisting
- LDAP error displayed on the login screen

### Changed
- Login screen positioning and application of the common error div generator

### Fixed
- LDAP password character check regular expression - proper escaping

## [1.0.1] - 2018-03-27
### Added
- optional scrolling parameters (overflowX and overflowY) in the modal
- success tick after saving a modal

### Fixed
- style (print css, placeHolder/text visibility and modal scrolling)


## [1.0.0] - 2018-01-30
### Added
- PDOe class (extends PDO) and database connection through PDOe
- validateColumnNames and validateColumnName common function

### Changed
- core uses PDOe and prepared statements instead of deprecated mysql functions
- '\_\_construct()' functions as class constructors
- deprecated mysql_ function connection removed

### Fixed
- isset checks, etc (to avoid minor notifications)


## [0.5.0] - 2018-01-26
### Added
- SMTP allow self-signed SSL certificate (optionally)
### Changed
- SwiftMailer updated to 5.4.9
### Fixed
- SMTP allow self-signed SSL certificate
- isset checks, etc (to avoid minor notifications)

## [0.4.3] - 2018-01-09
### Added
- dropzone library
- addDropzone and addTypingSearch functions for the pages
### Fixed
- optional width parameter in the forms
- email field as textarea
- value setting for the typing search

## [0.4.2] - 2017-10-13
### Added
- 'file' type in the form class
- LDAP password characters whitelisting extended
### Changed
- table width and caption width in the forms don't have a default value and if they are not set, they won't be applied

## [0.4.1] - 2017-10-10
### Added
- print css for grayscaled eco printing

## [0.4.0] - 2017-10-06
### Added
- addColumns, addColumn, setColumns and deleteColumns methods for listing
- log folder
- rsaEncryption class for encrypting and decrypting data with RSA public key pairs
- LDAP logging for debugging
### Changed
- common classes and functions moved to 'classes' and 'functions' folders
### Fixed
- LDAP username can also contain underscore character

## [0.3.7] - 2017-09-29
### Added
- centerWrapperTag enabled optional property in the list
### Fixed
- ajaxModal executes javascripts in the script tags of the ajax content
- modal max-height and vertical scrolling
- orderList working in the ajaxModal content too, lists pass table id to the ordering also

## [0.3.6] - 2017-09-08
### Added
- additionalBackButtonFormValues property in the crudPage

## [0.3.5] - 2017-09-05
### Added
- JavaScript modal
- Basic documentation
- placeHolder option for the ajaxForm fields
- textarea field type in the ajaxForm
- common style for the disabled input fields
### Changed
- simpler style for the tables

## [0.3.4] - 2017-08-29
### Changed
- responsive 'always on top' menu
### Fixed
- logged in user div doesn't overlay the content

## [0.3.3] - 2017-08-22
### Added
- optional logging of successful and failed login tries

## [0.3.2] - 2017-08-18
### Added
- optional 'showControlButtonAlways' config parameter in the ajaxForm
- optional 'successResponseCallback' and 'errorResponseCallback' functions as config parameters in the ajaxForm
- 'enabled' property in the form, fields can be disabled in the whole form by this property

## [0.3.1] - 2017-08-11
### Added
- optional 'encryption' smtp config parameter for email sending functions

## [0.3.0] - 2017-08-10
### Added
- dropdown submenu feature in the top menu
- SwiftMailer library and common email sending functions, global constants
### Changed
- singleline textarea instead of text inputs in the forms by default (handles special characters)
- file modes
### Fixed
- ajaxForm created 'inputDiv' HTML tags instead of 'div' as containers for input fields
- possibility of turning off the 'show logged in user' feature

## [0.2.3] - 2017-08-04
### Added
- additional DB condition property in the crudPage
### Fixed
- load function in the DBE returns false in case of an error

## [0.2.2] - 2017-07-04
### Added
- numeric input step option in the forms and in the ajaxForms
### Changed
- numeric input spin buttons are hidden by default
### Fixed
- caseImgButtons didn't work in Firefox

## [0.2.1] - 2017-06-13
### Added
- Common password format check function

### Fixed
- Client-side MD5 hash javascript algorithm replaced due to invalid MD5 hash generation for strings containing special characters

## [0.2.0] - 2017-06-09
### Added
- ajaxForm.js - a JavaScript handler for smaller included forms using ajax request to save data
- getUrlAjax function as a common getter function of the logged in user's valid url for the ajax request

### Fixed
- Checking value of 'email' type field didn't work in forms (requirement of valid e-mail address format and domain)

## [0.1.2] - 2017-05-30
### Added
- Optional confirm modal can be set for image controls of listing (confirmQuestion value of the handler config)
- Confirm modal for crudPage delete operations

## [0.1.1] - 2017-05-16
### Added
- Show logged in user's name and username in the header (optional), with logout link moved here (optional)
- tableClass field in urlap class to set CSS class name for the form table
- caption table cells in the form table of the urlap class get 'caption' class

## [0.1.0] - 2017-05-12
### Added
- LDAP authentication - NOTE: you have to ensure the encryption of the LDAP passwords by yourself in this case (e.g. using SSL)
### Changed
- authentication client-side javascript is in a single js file (auth/auth.js) instead of being inside index.php
### Fixed
- Log in again option in case of expired session (any session / unauthenticated session) error

## [0.0.3] - 2017-05-05
### Fixed
- Linking of the css and the js files of the pages when using https urls

## [0.0.2] - 2017-05-02
### Fixed
- Login urls depending on INTRANET_ADDRESS defined constant

## [0.0.1] - 2017-04-18
### Added
- Software in the state before versioning
- This changelog file

### Changed
- Versioning started
- CaseImgButtons in crudPage and listing
- crudPage edit and delete buttons as caseImgButtons
- crudPage changeImgHandlersOnly flag to prevent printing page from any caseImgButtonHandler optionally

### Fixed
- Default timezone set to Europe/Budapest (newer php versions / certain webserver configurations required this setting)