<?php
//if (!isset($_SESSION)) { session_start(); }
include_once("connection.php");
if (LDAP_ENABLED) {
	require_once(dirname(__FILE__) . "/auth/verifyLDAP.php");
	require_once(dirname(__FILE__) . "/auth/getIsLDAPUser.php");
}

$uname = $_POST["uname"];
$pass = $_POST["pass"];
$salt = $_SESSION["salt"];
$salt2 = $_SESSION["salt2"];
$c = "SELECT ".USER_TABLE_ID." as id, ".USER_USERNAME_COLUMN." as username";
$user_mezok[] = "username";
$definedConsts = get_defined_constants(true)["user"];
if (isset($definedConsts["USER_FULL_NAME_FIELD"]) && $definedConsts["USER_FULL_NAME_FIELD"] !== "") {
	//$c.= ", ".USER_FULL_NAME_FIELD;
	$user_mezok[] = USER_FULL_NAME_FIELD;
}
foreach ($kepessegek as $k => $e) {
	$c.= ", $k";
}
foreach ($user_mezok as $e) {
	$c.= ", $e";
}
$c.= (MULTI_LANG)?", lang":"";
$c.= " FROM ".USER_TABLE." WHERE ";
$isLDAP = LDAP_ENABLED
	? getIsLDAPUser($uname, $error)
	: false;
$c_getUserName = $c;
$c.= $isLDAP
	? "username = :uname;"
	: "MD5(CONCAT(:salt,username)) = :uname AND MD5(CONCAT(:salt2,password)) = :pass;";

// command for getting the username from the DB in order to logging successful and failed login tries:
$c_getUserName.= $isLDAP
	? "username = :uname;"
	: "MD5(CONCAT(:salt,username)) = :uname;";

try {
	if ($error) {
		saveLog(false, $error);
		header("location: index.php?logout=true&wr=true");
		exit(0);
	}
	$statement = $pdo -> filteredExecute($c, array(
		":uname" => $uname,
		":pass" => $pass,
		":salt" => $salt,
		":salt2" => $salt2
	));
	$count = $statement -> rowCount();
	if ($count === 1) {
		// If it is an LDAP user, then verifying LDAP username and password
		if ($isLDAP) {
			$logData = array("date" => date("Y.m.d. H:i:s"), "success" => true);
			$logData["server"] = $_SERVER;
			$logData["request"] = $_REQUEST;
			if (!LDAP_verify($uname, $pass, $errorMsg)) {
				$logData["success"] = false;
				$logData["error"] = $errorMsg;
				saveLDAPLog($logData);
				saveLog(false, $errorMsg);
				header("location: index.php?logout=true&wr=true&err=" . urlencode($errorMsg));
				exit(0);
			}
			saveLDAPLog($logData);
			$uname = MD5($salt.$uname);
		}
		$_SESSION["isLDAP"] = $isLDAP;

		$reg = $statement -> fetch();
		foreach ($kepessegek as $k => $e) {
			$_SESSION[$k] = $reg[$k];
		}
		foreach ($user_mezok as $e) {
			$_SESSION[$e] = $reg[$e];
		}
		if (MULTI_LANG) {
			$_SESSION["lang"] = $reg["lang"];
		}
		if ($isLDAP) {
			saveLog(true, "", $_POST["uname"]);
		} else {
			saveLog(true);
		}
		header("location: ".INTRANET_ADDRESS."/?session_id=$uname&page=index");
	} else {
		saveLog(false);
		header("location: index.php?logout=true&wr=true");
	}
} catch (PDOException $e) {
	errDiv($f["login_adatbazis_hiba"]);
}

function saveLDAPLog(&$logData) {
	if (!LDAP_LOG_ENABLED) {
		return;
	}
	if (!LDAP_LOG_FILE || LDAP_LOG_FILE === "") {
		return;
	}
	$enc = new rsaEncryption();
	$enc -> pubkey = LDAP_LOG_KEY;
	try {
		$encryptedData = $enc -> encrypt(json_encode($logData));
		file_put_contents(LDAP_LOG_FILE, $encryptedData."\n\n", FILE_APPEND);
	} catch (Exception $e) {

	}
}

function saveLog($success, $error = "", $originalUname = "") {
	global $uname, $salt, $pdo, $c_getUserName;
	if (!LOG_LOGINS) {
		return;
	}
	$user_id = 0;
	$username = "Unknown user";
	$originalUname = ($originalUname === "") ? $uname : $originalUname;
	try {
		$statement_getUserName = $pdo -> filteredExecute($c_getUserName, array(
			":uname" => $originalUname,
			":salt" => $salt
		));
	
		if ($statement_getUserName -> rowCount() === 1) {
			$u = $statement_getUserName -> fetch();
			$user_id = $u["id"];
			$username = $u["username"];
		}
		if ($error !== "") {
			$error = " Error message: ".$error;
		}
		$logMessageType = $success ? "success" : "failed";
		$logMessage = $success
			? "$username logged in."
			: "Unsuccessful login try: $username.";
		$logMessage.= $error;
		$cmd = "INSERT INTO ".LOG_TABLE." (type, message, user_id, username, HTTP_USER_AGENT, REMOTE_ADDR, REMOTE_HOST, REMOTE_PORT) VALUES(:logMessageType, :logMessage, :user_id, :username, '$_SERVER[HTTP_USER_AGENT]', '$_SERVER[REMOTE_ADDR]', '"
			.(isset($_SERVER["REMOTE_HOST"])
				? $_SERVER["REMOTE_HOST"]
				: gethostbyaddr($_SERVER["REMOTE_ADDR"]))
			."', '$_SERVER[REMOTE_PORT]')";
		$insertStatement = $pdo -> prepare($cmd);
		$insertStatement -> execute(array(
			":logMessageType" => $logMessageType,
			":logMessage" => $logMessage,
			":user_id" => $user_id,
			":username" => $username
		));
	} catch (PDOException $e) {

	}
}
?>