<?
function LDAP_verify($username, $password, &$errorMsg="") {

	if (empty($username) || empty($password)) {
		$errorMsg = "Username and password can't be an empty string!";
		return false;
	}

	//check for expected values (whitelisting):
	if (preg_match('/[^a-zA-Z0-9_]/',$username)) {
		$errorMsg = "Username must contain alphanumeric letters and '_' character only!";
		return false;
	}

	if (preg_match('/[^a-zA-Z0-9\x20' . preg_quote(LDAP_PASSWORD_CHARACTERS_WHITELIST) . ']/', $password)) {
		$errorMsg = "Password must contain alphanumeric letters, spaces and " . LDAP_PASSWORD_CHARACTERS_WHITELIST . " characters only!";
		return false;
	}

	//check for null byte (blacklisting):
	if (preg_match('/\x00/',$username) or preg_match('/\x00/',$password)) {
		$errorMsg = "Username and password cannot be a null byte string!";
		return false;
	} 

	$ldapConn = ldap_connect(LDAP_HOST, LDAP_PORT);
	if (!$ldapConn) {
		$errorMsg = "LDAP connection error.";
		return false;
	}
	// binding to ldap server
	ldap_set_option($ldapConn, LDAP_OPT_PROTOCOL_VERSION, 3);
	ldap_set_option($ldapConn, LDAP_OPT_NETWORK_TIMEOUT, 10);
	$ldapBind = ldap_bind($ldapConn, LDAP_BIND_RDN, LDAP_BIND_PASSWORD);

	// verify binding
	if (!$ldapBind) {
		$errorMsg = "LDAP bind failed...<br/>";
		$errorMsg.= ldap_errno($ldapConn).": ".ldap_error($ldapConn);
		return false;
	}

	//filter by user name
	$dn = LDAP_DN; //binda_dn = '(&(objectclass=person)(uid={}))'.format(username)
	$filter = "uid=".$username;

	$sr=ldap_search($ldapConn, $dn, $filter);
	if ($sr === false) {
		$errorMsg = "LDAP search error<br/>";
		$errorMsg.= ldap_errno($ldapConn).": ".ldap_error($ldapConn);
		return false;
	}

	$result = ldap_get_entries($ldapConn, $sr);

	if ($result["count"] !== 1) {
		$errorMsg = "Cannot get userinfo, number returned entries is ".$result["count"]." instead of 1 (username is incorrect).";
		return false;
	}
	$userinfo = $result[0];
	if (!isset($userinfo["dn"])) {
		$errorMsg = "Cannot get dn from userinfo!";
		return false;
	}
	$dn = $userinfo["dn"];

	$ldapBind = ldap_bind($ldapConn, $dn, $password);
	if (!$ldapBind) {
		$errorMsg = "Password incorrect!<br/>";
		$errorMsg.= ldap_errno($ldapConn).": ".ldap_error($ldapConn);
		return false;
	}
	return true;
	ldap_close($ldapConn);
}
?>