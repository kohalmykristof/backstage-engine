<?
	require_once(dirname(__FILE__) . "/getIsLDAPUser.php");
	$definedConsts = get_defined_constants(true)["user"];
	if (!isset($definedConsts["USER_LDAP_COLUMN"]) || !isset($definedConsts["USER_TABLE"]) || !isset($definedConsts["USER_USERNAME_COLUMN"])) {
		exit(0);
	}
	$isLDAP = getIsLDAPUser($_GET["username"], $error);
	$response = array(
		"isLDAPuser" => $isLDAP ? "true" : "false",
		"error" => $error ? "true" : "false"
	);
	print json_encode($response);
?>