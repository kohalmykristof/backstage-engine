function passResponse(salt, salt2, userSaltedPw, LDAPenabled) {

	var username = document.login.uname_t.value;
	var password = document.login.pass_t.value;

	if (LDAPenabled) {
		handleLDAPauth(username, password, function(isLDAPuser) {
			if (isLDAPuser) {
				passResponseSubmit(password, salt2, false);
				return;
			}

			handleAuth(username, password, salt, salt2, userSaltedPw);

		});
		return;
	}

	handleAuth(username, password, salt, salt2, userSaltedPw);

}

function handleAuth(username, password, salt, salt2, userSaltedPw) {

	document.hform.uname.value = MD5(salt + username);
	if (userSaltedPw) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var getUserSaltResponse;
				var userSalt;
				try {
					getUserSaltResponse = JSON.parse(this.responseText);
					userSalt = getUserSaltResponse.userSalt;
				} catch(err) {
					// invalid reply to AJAX request
					alert("Invalid response from server!");
					return;
				}
				password = MD5(password + userSalt);
				passResponseSubmit(password, salt2, true);
			}
		};
		xhttp.open("GET", "?ajax=getusersalt&username=" + username, true);
		xhttp.send();
		return;
	}
	passResponseSubmit(password, salt2, true);

}

function handleLDAPauth(username, password, callback) {

	document.hform.uname.value = username;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var getIsLDAPuserResponse;
			var isLDAPuser;
			try {
				getIsLDAPuserResponse = JSON.parse(this.responseText);
			} catch(err) {
				// invalid reply to AJAX request
				throw new Error("LDAP: invalid response from server!");
				return;
			}
			if (getIsLDAPuserResponse.error !== "false") {
				throw new Error("LDAP: error response received from server!");
			}
			if (getIsLDAPuserResponse.isLDAPuser !== "true" && getIsLDAPuserResponse.isLDAPuser !== "false") {
				throw new Error("LDAP: invalid response received from server! Required isLDAPuser field with a 'true' or 'false' string type value!");
			}
			isLDAPuser = getIsLDAPuserResponse.isLDAPuser;
			
			callback(isLDAPuser === "true");
		}
	};
	xhttp.open("GET", "?ajax=getIsLDAPuser&username=" + username, true);
	xhttp.send();

}

function passResponseSubmit(password, salt2, saltedPassword) {

	document.hform.pass.value = saltedPassword
		? MD5(salt2 + password)
		: password;
	document.login.pass_t.value = "";
	document.hform.submit();

}