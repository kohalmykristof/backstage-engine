<?
	$definedConsts = get_defined_constants(true)["user"];
	if (!isset($definedConsts["USER_SALT_COLUMN"]) || !isset($definedConsts["USER_TABLE"]) || !isset($definedConsts["USER_USERNAME_COLUMN"])) {
		exit(0);
	}
	$userSalt = getUserSalt($_GET["username"]);
	$response = array(
		"userSalt" => $userSalt
	);
	print json_encode($response);
	function getUserSalt($username) {
		global $pdo;
		try {
			$statement = $pdo -> prepare("SELECT ".USER_SALT_COLUMN." as hash_salt FROM ".USER_TABLE." WHERE ".USER_USERNAME_COLUMN."=:username;");
			$statement -> execute(array(":username" => $username));
			if ($statement -> rowCount() !== 1) {
				return false;
			}
			$u = $statement -> fetch();
			return $u["hash_salt"];
		} catch (PDOException $e) {
			return false;
		}
	}
?>