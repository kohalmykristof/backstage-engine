<?
function getIsLDAPUser($username, &$error) {
	global $pdo;
	$error = false;
	try {
		$statement = $pdo -> prepare(
			"SELECT ".USER_LDAP_COLUMN." as LDAP FROM ".USER_TABLE." WHERE ".USER_USERNAME_COLUMN."=:username;"
		);
		$statement -> execute(array(":username" => $username));
		if ($statement -> rowCount() !== 1) {
			return false;
		}
		$u = $statement -> fetch();
		return $u["LDAP"] === "1";
	} catch (PDOException $e) {
		$error = true;
		return false;
	}
}
?>