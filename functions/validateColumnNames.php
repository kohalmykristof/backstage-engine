<?
function validateColumnNames($columnNames) {
	foreach ($columnNames as $k => $e) {
		validateColumnName($e);
	}
}

function validateColumnName($columnName) {
	$columnNameRegexp = "/^[a-z0-9_@.`]*$/i";
	if (!preg_match($columnNameRegexp, $columnName)) {
		throw new Exception("Invalid column name: $columnName!");
	}
}
?>