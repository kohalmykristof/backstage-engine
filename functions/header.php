<?
function printHeaderAdd($additional_header_information, $menu = true) {
	global $oldalak, $f;
?><html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<? print $additional_header_information; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="noindex, nofollow" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<title><? print TITLE; if(isset($oldalak[$_GET["page"]])) { print ($oldalak[$_GET["page"]]->cim!="")?" - ".$oldalak[$_GET["page"]]->cim:""; }?></title>
	<link href="<? print INTRANET_ADDRESS; ?>/lib/jsmodal/css/jsmodal-dark-K2.css" rel="stylesheet" type="text/css" />
	<link href="<? print INTRANET_ADDRESS; ?>/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<? print INTRANET_ADDRESS; ?>/css/print.css" rel="stylesheet" type="text/css" />
	<link href="<? print INTRANET_ADDRESS; ?>/admin/css/custom.css" rel="stylesheet" type="text/css" />
<?
	if (isset($_GET["page"]) && isset($oldalak[$_GET["page"]])) {
		if (is_array($oldalak[$_GET["page"]] -> styles)) {
			foreach ($oldalak[$_GET["page"]] -> styles as $e) {
				$css_url = INTRANET_ADDRESS."/admin/css/$e";
				if (substr(strtolower($e),0,7) === "http://" || substr(strtolower($e),0,8) === "https://"){
					$css_url = $e;
				}
				?><link href="<? print $css_url; ?>" rel="stylesheet" type="text/css" /><?
			}
		}
	}
?>
</head>
<body>
<script src="<? print INTRANET_ADDRESS; ?>/lib/jsmodal/js/jsmodal-1.0d-K2.js"></script>
<script src="<? print INTRANET_ADDRESS; ?>/confirmSubmitForm.js"></script>
<?	
	if (isset($_GET["page"]) && isset($oldalak[$_GET["page"]]) && is_array($oldalak[$_GET["page"]] -> javascripts)) {
		foreach ($oldalak[$_GET["page"]] -> javascripts as $e) {
			$script_url = INTRANET_ADDRESS."/admin/js/$e";
			if (substr(strtolower($e),0,7) === "http://" || substr(strtolower($e),0,8) === "https://") {
				$script_url = $e;
			}
			print "<script src=\"$script_url\"></script>";
		}
	}
?>
<div id="wrapper">
	<div id="header">
		<ul id="navigation">
			<? if ($menu) { printMenu(); } ?>
		</ul>
<?
	if (SHOW_LOGGED_IN_USER && isset($_SESSION["username"]) && $_SESSION["username"] && isset($_GET["page"])) {
		$fullName = ((USER_FULL_NAME_FIELD !== "") && isset($_SESSION[USER_FULL_NAME_FIELD]))
			? $_SESSION[USER_FULL_NAME_FIELD] : "";
		print "<div id='logged_in_user'>
	<div class='logged_in_as_caption'>".$f["logged_in_as"].":</div>
	<div class='logged_in_as_name'>".$fullName." (".$_SESSION["username"].")</div>";
		if (LOGOUT_AT_LOGGED_IN_USER) {
			print "<div class='logout_link'><a href='".INTRANET_ADDRESS."/index.php?logout=true'>$f[logout]</a></div>";
		}
		print "</div>";
	}
?>
	</div>
	<div id="body">
		<center>
<?
}
function printHeader($menu = true) {
	printHeaderAdd("", $menu);
}
?>