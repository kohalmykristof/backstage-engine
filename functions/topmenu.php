<?
function printMenu() {
	global $f;
	global $oldalak;
	if (!checkLogin()) {
		print "</ul>";
		print "</div>";
		print "<div id=\"body\">";
		if (DEBUG) {
			print_r($_SESSION);
		}
		print "<center>";
		if (!isset($_SESSION["salt"]) || !isset($_GET["session_id"]) || $_SESSION["salt"] === "" || $_GET["session_id"] === "") {
			errDiv($f["hibas_session_azonositas"]);
			print "<a href='".INTRANET_ADDRESS."/index.php?logout=true'><button>$f[login_ujra]</button></a>";
		} else {
			errDiv($f["hibas_login"]);
			print "<a href='".INTRANET_ADDRESS."/index.php?logout=true'><button>$f[login_ujra]</button></a>";
		}
		print("</center></div></body></html>");
		exit(0);
	}
	$menu = array();
	$i = 0;
	foreach ($oldalak as $k => $e) {
		if (checkCapability($k)) {
			if ($e -> menu !== "") {
				$menuItem = "<li".(($_GET["page"] === $k) ? " class='selected'" : "")."><a href='".getUrl($k)."'>".$e -> menu."</a></li>";
				if ($e -> dropDownGroup !== "") {
					if (!isset($menu[$e -> dropDownGroup])) {
						$menu[$e -> dropDownGroup] = array("type" => "dropDown", "menuItem" => "", "dropDownGroup" => $e -> dropDownGroup, "dropDownCaption" => $e -> dropDownCaption);
					}
					$menu[$e -> dropDownGroup]["menuItem"].= $menuItem;
					continue;
				}
				$menu[$i] = array("type" => "single", "menuItem" => $menuItem);
				$i++;
			}
		}
	}
	foreach ($menu as $k => $e) {
		if ($e["type"] === "single") {
			print $e["menuItem"];
			continue;
		}
		if ($e["type"] === "dropDown") {
			print "<li class='dropdown'><span>".$e["dropDownCaption"]."</span><div class='dropdown_content dropdown_menu'><ul>".$e["menuItem"]."</ul></div></li>";
		}
	}
	if (!LOGOUT_AT_LOGGED_IN_USER) {
		echo "<li".(($selected_ === "logout") ? " class='selected'" : "")."><a href='".INTRANET_ADDRESS."/index.php?logout=true'>$f[logout]</a></li>";
	}
}
?>