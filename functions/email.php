<?
require_once(dirname(__FILE__) . "/../lib/swiftmailer-5.4.9/swift_required.php");

function email_attachment($from_email, $from_name, $to_email, $to_name, $subject, $msg, $msg_text, $attachment, $customSmtpConfig = "") {
	$smtpConfig = array(
		"host" => SMTP_HOST,
		"port" => SMTP_PORT,
		"encryption" => null,
		"user" => SMTP_USER,
		"pass" => SMTP_PASS,
		"allow_self_signed" => SMTP_SELF_SIGNED_SSL
	);
	if (is_array($customSmtpConfig)) {
		if (isset($customSmtpConfig["host"])) { $smtpConfig["host"] = $customSmtpConfig["host"]; }
		if (isset($customSmtpConfig["port"])) { $smtpConfig["port"] = $customSmtpConfig["port"]; }
		if (isset($customSmtpConfig["encryption"])) { $smtpConfig["encryption"] = $customSmtpConfig["encryption"]; }
		if (isset($customSmtpConfig["user"])) { $smtpConfig["user"] = $customSmtpConfig["user"]; }
		if (isset($customSmtpConfig["pass"])) { $smtpConfig["pass"] = $customSmtpConfig["pass"]; }
		if (isset($customSmtpConfig["self_signed_ssl"])) { $smtpConfig["self_signed_ssl"] = $customSmtpConfig["self_signed_ssl"]; }
	}
	
	// Create the message
	$message = Swift_Message::newInstance()

		// Give the message a subject
		->setSubject($subject)

		// Set the From address with an associative array
		->setFrom(array($from_email => $from_name))

		// Set the To addresses with an associative array
		->setTo(array($to_email => $to_name))

		// Give it a body
		->setBody($msg, 'text/html')

		// And optionally an alternative body
		->addPart($msg_text)
		;
	
	if ($attachment !== "") {
		$message -> attach(Swift_Attachment::fromPath($attachment));
	}
	// Create the Transport
	$transport = Swift_SmtpTransport::newInstance($smtpConfig["host"], $smtpConfig["port"], $smtpConfig["encryption"]);

	if ($smtpConfig["self_signed_ssl"]) {
		$sslOptions = array(
			"ssl" => array(
				"allow_self_signed" => true,
				"verify_peer" => false,
				"verify_peer_name" => false
			)
		);
		$transport -> setStreamOptions($sslOptions);
	}

	$transport
		->setUsername($smtpConfig["user"])
		->setPassword($smtpConfig["pass"])
		;

	// Create the Mailer using your created Transport
	$mailer = Swift_Mailer::newInstance($transport);

	// Send the message
	$result = $mailer -> send($message);
}

function email($from_email, $from_name, $to_email, $to_name, $subject, $msg, $msg_text, $smtpConfig = "") {
	email_attachment($from_email, $from_name, $to_email, $to_name, $subject, $msg, $msg_text, "", $smtpConfig);
}

function replaceHtmlTag($html, $tag, $replacementOpen = "", $replacementClose = "") {
	$tag = preg_quote($tag);
	$open = "/<".$tag."[^>]*>/mi";
	$close = "/<\/$tag>/mi";
	return preg_replace([$open, $close], [$replacementOpen, $replacementClose], $html);
}

function emailHtmlToText($html) {
	$html = replaceHtmlTag($html, "b");
	$html = replaceHtmlTag($html, "i");
	$html = replaceHtmlTag($html, "h1", "\n", "\n");
	$html = replaceHtmlTag($html, "h2", "\n", "\n");
	$html = replaceHtmlTag($html, "h3", "\n", "\n");
	$html = replaceHtmlTag($html, "h4", "\n", "\n");
	$html = replaceHtmlTag($html, "h5", "\n", "\n");
	$html = replaceHtmlTag($html, "h6", "\n", "\n");
	$html = replaceHtmlTag($html, "table", "\n", "\n");
	$html = replaceHtmlTag($html, "tr", "", "\n");
	$html = replaceHtmlTag($html, "th", "", "\t");
	$html = replaceHtmlTag($html, "td", "", "\t");
	$replacements = [
		"<br/>" => "\n",
		"<br>" => "\n"
	];
	return str_replace(
		array_keys($replacements),
		$replacements,
		$html
	);
}
?>