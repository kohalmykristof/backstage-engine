<?
session_save_path(dirname(__FILE__) . '/tmp');
ini_set('session.gc_maxlifetime', 7*24*60*60);
ini_set("session.cookie_lifetime", 7*24*60*60);
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 1);
ini_set('session.cookie_secure', false);
ini_set('session.use_only_cookies', true);
session_set_cookie_params(7*24*60*60,'/');

session_start();
include_once(dirname(__FILE__) . '/feliratok.php');
if(!isset($_SESSION["lang"])) {
	$_SESSION["lang"]=0;
}
foreach($feliratok as $k => $e) {
	$f[$k]=$e[$_SESSION["lang"]];
}
include_once(dirname(__FILE__) . '/classes/PDOe.php');
include_once(dirname(__FILE__) . '/classes/oldal.php');
include_once(dirname(__FILE__) . '/ajax_handlers_default.php');
include_once(dirname(__FILE__) . '/global_parameters.php');
include_once(dirname(__FILE__) . '/global_parameters_default.php');
include_once(dirname(__FILE__) . '/functions/email.php');
include_once(dirname(__FILE__) . '/functions/header.php');
include_once(dirname(__FILE__) . '/functions/topmenu.php');
include_once(dirname(__FILE__) . '/functions/footer.php');
include_once(dirname(__FILE__) . '/functions/validateColumnNames.php');
include_once(dirname(__FILE__) . '/classes/DBE.php');
include_once(dirname(__FILE__) . '/classes/felsorol.php');
include_once(dirname(__FILE__) . '/classes/urlap.php');
include_once(dirname(__FILE__) . '/lib/fpdf/fpdf.php');
include_once(dirname(__FILE__) . '/functions/pdf.php');
include_once(dirname(__FILE__) . '/classes/rsa.php');
include_once(dirname(__FILE__) . '/classes/crudPage.php');
if (file_exists(dirname(__FILE__) . '/admin/custom_includes.php')) {
	include_once(dirname(__FILE__) . '/admin/custom_includes.php');
}

/* $request_url = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
if (strpos ($request_url, INTRANET_ADDRESS) === false) {
	throw new Exception("Given INTRANET_ADDRESS (".INTRANET_ADDRESS.") config parameter doesn't match to request url (".$request_url.")!");
} */

try {
	$pdo = new PDOe("mysql:host=".db_host.";dbname=".db_adatbazis.";charset=utf8", db_felhasznalo, db_jelszo);
	$pdo -> exec("SET NAMES 'utf8';");
	$pdo -> exec("SET CHARACTER SET 'utf8';");
	$pdo -> exec("SET collation_connection = 'utf8_hungarian_ci';");
} catch (PDOException $e) {
	print "<br/>";
	if ($e -> getCode() === 1045) {
		die($f["db_auth_error"]);
	}
	if ($e -> getCode() === 1049) {
		die($f["adatbazis_nem_talalhato"]);
	}
	die($f["adatbazis_hiba"]." ".$e -> getMessage());
}

if (DEBUG) {
	ini_set('display_errors', 1);
}

// deprecated, generating '_orig' arrays only for the compatibility of prior codes using that:
$_POST_orig = $_POST;
$_GET_orig = $_GET;
$_COOKIE_orig = $_COOKIE;
$_REQUEST_orig = $_REQUEST;

function removeslashes($string) {
    //$string=implode("",explode("\\",$string));
    //return stripslashes(trim($string));
	return str_replace("\\","",$string);
}

function generateSalt($max = 15, $numbers=false) {
	$characterList = $numbers?"0123456789":"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
	$i = 0;
	$salt = "";
	while ($i < $max) {
		$salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
		$i++;
	}
	return $salt;
}

function checkLogin() {
	global $pdo;
	$salt = $_SESSION["salt"];
	try {
		$c = $pdo -> prepare(
			"SELECT ".USER_TABLE_ID." as id FROM ".USER_TABLE."
			WHERE MD5(CONCAT(:salt, username))=:session_id;"
		);
		$c -> execute(array(
			":salt" => $salt,
			":session_id" => $_GET["session_id"]
		));
		if ($c -> rowCount() !== 1) {
			return false;
		}
		$res = $c -> fetch();
		$_SESSION["user_id"] = $res["id"];
		return true;
	} catch (PDOException $e) {
		return false;
	}
}

function checkPasswordFormat($password, &$errorMsg) {
	global $f;

	if (strlen($password) < PW_FORMAT_MIN_LENGTH) {
		$errorMsg = str_replace("%NUM%", PW_FORMAT_MIN_LENGTH, $f["pw_format_min_length_error"]);
		return false;
	}

	$numOfLetters = 0;
	$numOfNumbers = 0;
	$numOfNonAlphaNumerics = 0;
	for ($i = 0; $i < strlen($password); $i++) {
		$c = $password[$i];
		if (preg_match("/[^A-Za-z0-9]/", $c)) {
			// non-alphanumeric character
			$numOfNonAlphaNumerics++;

			if (PW_FORMAT_DISABLE_INJECTION_CHARS && ($c === "\\" || $c === "'" || $c === "\"")) {
				$errorMsg = $f["pw_format_injection_chars_error"];
				return false;
			}
			continue;
		}
		if (preg_match("/[0-9]/", $c)) {
			$numOfNumbers++;
			continue;
		}
		$numOfLetters++;
	}

	if ($numOfNumbers < PW_FORMAT_MIN_NUMBERS) {
		$errorMsg = str_replace("%NUM%", PW_FORMAT_MIN_NUMBERS, $f["pw_format_min_numbers_error"]);
		return false;
	}

	if ($numOfLetters < PW_FORMAT_MIN_LETTERS) {
		$errorMsg = str_replace("%NUM%", PW_FORMAT_MIN_LETTERS, $f["pw_format_min_letters_error"]);
		return false;
	}

	if ($numOfNonAlphaNumerics < PW_FORMAT_MIN_NONALPHANUMERICS) {
		$errorMsg = str_replace("%NUM%", PW_FORMAT_MIN_NONALPHANUMERICS, $f["pw_format_min_nonalphanumerics_error"]);
		return false;
	}

	return true;
}

function getUrl($page) {
	return INTRANET_ADDRESS . "/?session_id=$_GET[session_id]&page=$page";
}

function getUrlAjax($ajaxHandlerName) {
	return INTRANET_ADDRESS . "/?session_id=$_GET[session_id]&ajax=".$ajaxHandlerName;
}

function checkCapability($page) {
	global $oldalak;
	if ($page === "") {
		return false;
	}
	if (!isset($oldalak[$page])) {
		return false;
	}
	if (!is_array($oldalak[$page] -> kepessegek)) {
		return false;
	}
	foreach ($oldalak[$page] -> kepessegek as $e) {
		if ($_SESSION[$e] !== '1') {
			return false;
		}
	}
	return true;
}

function checkAjaxCapability($ajax) {
	global $ajaxHandlers;
	if ($ajax=="") {
		return false;
	}
	if (!isset($ajaxHandlers[$ajax])) {
		return false;
	}
	if (!is_array($ajaxHandlers[$ajax]["capabilities"])) {
		return false;
	}
	foreach ($ajaxHandlers[$ajax]["capabilities"] as $e) {
		if ($_SESSION[$e] !== '1') {
			return false;
		}
	}
	return true;
}

function msgDiv($msg, $toPrint=true) {
	$html = "<div class='uzenet'>$msg</div>";
	if ($toPrint) {
		print $html;
	}
	return $html;
}

function errDiv($err, $toPrint=true) {
	$html = "<div class='hibaUzenet'>$err</div>";
	if ($toPrint) {
		print $html;
	}
	return $html;
}

function slugify($text) {
	// replace non letter or digits by -
	$text = preg_replace('~[^\pL\d]+~u', '-', $text);

	// transliterate
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	// remove unwanted characters
	$text = preg_replace('~[^-\w]+~', '', $text);

	// trim
	$text = trim($text, '-');

	// remove duplicate -
	$text = preg_replace('~-+~', '-', $text);

	// lowercase
	$text = strtolower($text);

	if (empty($text)) {
		return 'n-a';
	}

	return $text;
}
?>