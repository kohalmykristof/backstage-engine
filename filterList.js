function createFilterList(config) {
	if (!config) {
		throw new Error("config is mandatory!");
	}
	if (typeof config.filters !== "object") {
		throw new Error("config.filters is mandatory and it has to be an object!");
	}
	if (!config.parentElement) {
		throw new Error("config.parentElement is mandatory!");
	}
	for (var field in config.filters) {
		if ((typeof config.filters[field].tdClass !== "string") && (typeof config.filters[field].filterFunction !== "function")) {
			throw new Error("Every field property of config.filters has to have a tdClass string type or a filterFunction function type property! Neither can be found for field '" + field + "'!");
		}
		if (typeof config.filters[field].tdClass === "string" && (!Array.isArray(config.filters[field].chkValues))) {
			throw new Error("Field properties of config.filters with given tdClass property has to have a chkValues property also which has to be an array!");
		}
	}

	var filters = config.filters;
	var parentElement = config.parentElement;
	var tableId = config.tableId;
	var exactMatch = config.exactMatch;

	for (var field in filters) {
		if (typeof filters[field].filterFunction !== "function") {
			filters[field].filterFunction = createDefaultFieldFilter(field, filters[field].tdClass, filters[field].chkValues);
		}
	}

	addChks();

	function addChks() {
		var filterDiv = document.createElement("div");
		filterDiv.setAttribute("class", "filter_div");
		for (var field in filters) {
			var fieldDiv = document.createElement("div");
			fieldDiv.setAttribute("class", "filter_field_div");
			var titleDiv = document.createElement("div");
			titleDiv.setAttribute("class", "filter_title_div");
			titleDiv.innerHTML = filters[field].title || "";
			fieldDiv.appendChild(titleDiv);
			var chksDiv = document.createElement("div");
			chksDiv.setAttribute("class", "filter_chks_div");
			for (var i = 0; i < filters[field].chkValues.length; i += 1) {
				var chkDiv = document.createElement("div");
				chkDiv.setAttribute("class", "filter_chk_div");
				var chkInput = document.createElement("input");
				chkInput.setAttribute("class", "filter_chk_input");
				chkInput.setAttribute("type", "checkbox");
				chkInput.setAttribute("value", filters[field].chkValues[i].value);
				chkInput.checked = true;
				chkInput.addEventListener("click", filter);
				filters[field].chkValues[i].input = chkInput;
				chkDiv.appendChild(chkInput);
				var labelSpan = document.createElement("span");
				labelSpan.setAttribute("class", "filter_chk_label");
				labelSpan.innerHTML = filters[field].chkValues[i].caption;
				chkDiv.appendChild(labelSpan);
				chksDiv.appendChild(chkDiv);
				if (i !== filters[field].chkValues.length - 1) {	
					chksDiv.appendChild(document.createTextNode(" "));
					var dividerSpan = document.createElement("span");
					dividerSpan.setAttribute("class", "filter_chk_divider");
					dividerSpan.innerHTML = "|";
					chksDiv.appendChild(dividerSpan);
					chksDiv.appendChild(document.createTextNode(" "));
				}
			}
			fieldDiv.appendChild(chksDiv);
			filterDiv.appendChild(fieldDiv);
		}
		parentElement.appendChild(filterDiv);
	}

	function createDefaultFieldFilter(field, tdClass, chkValues) {
		return function(tableRow) {
			var columns = tableRow.getElementsByClassName(tdClass);
			for (var j = 0; j < columns.length; j += 1) {
				var val = columns[j].innerHTML;
				for (var i = 0; i < filters[field].chkValues.length; i += 1) {
					var chkInput = filters[field].chkValues[i].input;
					if (!chkInput.checked) {
						continue;
					}
					var filterVal = chkInput.value;
					if (filterVal === val) {
						return true;
					}
					if (!exactMatch && val.indexOf(filterVal) !== -1) {
						return true;
					}
				}
			}
			return false;
		};
	}

	function filter() {
		var tableRoot = document.getElementById(tableId) || document;
		var tableRows = tableRoot.getElementsByTagName("tr");
		if (typeof tableRows.length !== "number" || tableRows.length < 2) {
			//return if table is empty or contains a header row only
			return;
		}
		if (!tableRows[0]) {
			throw new Error("First row not found!");
		}
		for (var i = 1; i < tableRows.length; i += 1) {
			var display = "";
			var tableRow = tableRows[i];
			for (var field in filters) {
				if (!filters[field].filterFunction(tableRow)) {
					display = "none";
					break;
				}
			}
			tableRow.style.display = display;
		}
	}

	return {
		filter: filter
	};

}