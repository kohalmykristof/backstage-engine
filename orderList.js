function createOrderList(config) {
	if (!config) {
		throw new Error("config is mandatory!");
	}
	if (typeof config.tdClass !== "string" || config.tdClass === "") {
		throw new Error("config is mandatory and it has to be a non-empty string!");
	}
	var tableId = config.tableId;
	var tdClass = config.tdClass;
	var desc = config.desc || false;
	var buttonId = config.buttonId;

	var sortByName = function(a, b) {
		return (desc?(-1):(1))*a.sortVal.localeCompare(b.sortVal);
	};

	var order = function() {
		var tableRoot = document.getElementById(tableId) || document;
		var column = tableRoot.getElementsByClassName(tdClass);
		if (typeof column.length !== "number" || column.length === 0) {
			return;
		}
		if (!column[0].parentElement) {
			throw new Error("First row not found!");
		}
		if (!column[0].parentElement.parentElement) {
			throw new Error("Table body not found!");
		}
		var tableBody = column[0].parentElement.parentElement;
		if (tableBody.nodeName !== "TABLE" && tableBody.nodeName !== "TBODY") {
			throw new Error("Assumed table body node's nodeName is not 'TABLE' or 'TBODY'!");
		}
		var tableElements = [];
		for (var i = 0; i < column.length; i += 1) {
			if (column[i].nodeName !== "TD") {
				continue;
			}
			var tableRow = column[i].parentElement;
			if (tableRow.nodeName !== "TR") {
				continue;
			}
			var sortVal = column[i].innerHTML.toLowerCase() + tableRow.innerHTML.toLowerCase();
			//tableBody.removeChild(tableRow);
			tableElements.push({
				sortVal: sortVal,
				tableRow:tableRow
			});
		}
		tableElements.sort(sortByName);
		for (var i = 0; i < tableElements.length; i += 1) {
			tableBody.appendChild(tableElements[i].tableRow);
		}
	};

	var clickEventListenerAdded = false;
	var addClickEventListener = function() {
		if (clickEventListenerAdded) {
			return;
		}
		var button = document.getElementById(buttonId);
		if (button) {
			clickEventListenerAdded = true;
			button.addEventListener("click", order);
		}
	};

	document.addEventListener("DOMContentLoaded", addClickEventListener);
	setTimeout(addClickEventListener, 0);

	/* return {
		order: order
	}; */
}