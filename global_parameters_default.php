<?
	ini_set('date.timezone', 'Europe/Budapest');
	$consts = array(
		"TESZT" => false,
		"DEBUG" => true,
		"db_host" => "localhost",
		"db_felhasznalo" => "user",
		"db_jelszo" => "pass",
		"db_adatbazis" => "db",
		"SMTP_HOST" => "",
		"SMTP_PORT" => 465,
		"SMTP_USER" => "",
		"SMTP_PASS" => "",
		"SMTP_SELF_SIGNED_SSL" => false,
		"USER_TABLE" => "users",
		"LOG_TABLE" => "users_log",
		"LOG_LOGINS" => false,
		"USER_TABLE_ID" => "id",
		"USER_FULL_NAME_FIELD" => "",
		"SHOW_LOGGED_IN_USER" => false,
		"LOGOUT_AT_LOGGED_IN_USER" => false,
		"INTRANET_ADDRESS" => "http://backstageengine.hu:7777",
		"TITLE" => "Backstage Engine",
		"MULTI_LANG" => false,
		"USER_SALTED_PW" => false,
		"USER_SALT_COLUMN" => "hash_salt",
		"USER_USERNAME_COLUMN" => "username",
		"LDAP_ENABLED" => false,
		"LDAP_HOST" => "",
		"LDAP_PORT" => 389,
		"LDAP_BIND_RDN" => "",
		"LDAP_BIND_PASSWORD" => "",
		"LDAP_DN" => "",
		"USER_LDAP_COLUMN" => "LDAP",
		"LDAP_PASSWORD_CHARACTERS_WHITELIST" => "!@#$%^&*().,:;-=[]",
		"LDAP_LOG_ENABLED" => false,
		"LDAP_LOG_FILE" => "",
		"LDAP_LOG_KEY" => "",
		"PW_FORMAT_MIN_LENGTH" => 5,
		"PW_FORMAT_MIN_NUMBERS" => 0,
		"PW_FORMAT_MIN_LETTERS" => 0,
		"PW_FORMAT_MIN_NONALPHANUMERICS" => 0,
		"PW_FORMAT_DISABLE_INJECTION_CHARS" => false
	);

	$definedConsts = get_defined_constants(true)["user"];
	foreach ($consts as $k => $e) {
		if (!isset($definedConsts[$k])) {
			define($k, $e);
		}
	}
	
	if (!isset($languages)) { $languages=array(); }
	if (!isset($kepessegek)) { $kepessegek = array(); }
	if (!isset($user_mezok)) { $user_mezok=array(); }
	if (!isset($oldalak)) {
		$oldalak = array(
			"index"=>new oldal("Main","Main",array())
		);
	}
?>