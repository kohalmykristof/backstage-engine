<?
try {
	include_once("connection.php");
	if (isset($_GET["logout"]) && $_GET["logout"]==="true") {
		session_destroy();
		session_start();
	}
	if (isset($_GET["page"])){
		if (!isset($oldalak[$_GET["page"]])) {
			include_once(dirname(__FILE__) . "/404.php");
			die();
		}
		if (!checkCapability($_GET["page"])) {
			printHeader();
			errDiv($f[nincs_jogosultsag]);
			printFooter();
			die();
		}
		include_once(dirname(__FILE__) . "/admin/$_GET[page].php");
	} else if (isset($_GET["ajax"])) {
		if(!isset($ajaxHandlers[$_GET["ajax"]])){
			print "INVALID REQUEST";
			die();
		}
		if(!checkAjaxCapability($_GET["ajax"])){
			print $f[nincs_jogosultsag];
			die();
		}
		include_once($ajaxHandlers[$_GET["ajax"]]["handler"]);
		exit(0);
	} else {
		$_SESSION["uname"]="";
		$_SESSION["pass"]="";
		$salt=generateSalt(8);
		$_SESSION["salt"]=$salt;
		$salt2=generateSalt(8);
		$_SESSION["salt2"]=$salt2;
		printLoginPage();
	}
} catch (Exception $e) {
	printHeader(false);
	errDiv($e->getMessage());
	printFooter();
}

function printLoginPage(){
	global $f;
	global $salt, $salt2;
	$additional_header='<script language="javascript" src="'.INTRANET_ADDRESS.'/auth/md5.js"></script>
<script src="'.INTRANET_ADDRESS.'/auth/auth.js" language="javascript"></script>';
	printHeaderAdd($additional_header, false);
	?>
	<style>
		body{
			overflow-y: hidden;
		}
		#body{
			width: 100%;
			height: 100%;
			vertical-align: middle;
		}
	</style>
	<div id="login">
	<?
		if (isset($_GET["wr"]) && $_GET["wr"] === "true") {
			errDiv("$f[hibas_login]".(isset($_GET["err"]) ? "<br/>$_GET[err]" : ""));
		}
	?>
	<table width="300" style="background-color: #CCCCCC; border-spacing: 1px;">
	<tr>
	<form name="login" action="<? print $PHP_SELF; ?>?session_id=<? print session_id(); ?>" method="post">
	<td>
	<table width="100%">
	<tr>
	<td colspan="2" align="center"><strong><? print $f["login"]; ?></strong></td>
	</tr>
	<tr>
	<td width="78" align="right"><? print $f["login_felhasznalonev"]; ?>: </td>
	<td width="294"><input name="uname_t" type="text"></td>
	</tr>
	<tr>
	<td align="right"><? print $f["login_jelszo"]; ?>: </td>
	<td><input name="pass_t" type="password"></td>
	</tr>
	<tr>
	<td colspan="2" align="center">
	<input onClick="passResponse('<? print $salt; ?>', '<? print $salt2; ?>', <? print USER_SALTED_PW === true ? "true" : "false"; ?>, <? print LDAP_ENABLED ? "true" : "false"; ?>); return false;" type="submit" name="submitbtn" value="<? print $f["login"]; ?>"></form>
	<form action="<? print INTRANET_ADDRESS; ?>/login.php?session_id=<? print session_id(); ?>" METHOD="POST" name="hform">
		<input type="hidden" name="uname">
		<input type="hidden" name="pass">
	</form>
	</tr>
	</table>
	</td>
	</form>
	</tr>
	</table>
	</div>
<?
	printFooter();
}
?>