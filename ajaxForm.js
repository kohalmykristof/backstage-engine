function createAjaxForm(config){
	if (!config) {
		throw new Error("config is mandatory!");
	}
	if (typeof config.fields !== "object") {
		throw new Error("config.fields is mandatory and it has to be an object!");
	}
	if (typeof config.values !== "object") {
		throw new Error("config.values is mandatory and it has to be an object!");
	}
	if (typeof config.saveUrl !== "string") {
		throw new Error("config.saveUrl is mandatory and it has to be a string!");
	}
	if (!config.parentDiv) {
		throw new Error("config.parentDiv is mandatory!");
	}
	if (!config.saveControlButton) {
		throw new Error("config.saveControlButton is mandatory!");
	}

	var fields = config.fields;
	var values = config.values;
	var saveUrl = config.saveUrl;
	var parentDiv = config.parentDiv;
	var saveControlButton = config.saveControlButton;
	var showControlButtonAlways = config.showControlButtonAlways || false;
	var saveSuccessTick = config.saveSuccessTick || document.createElement("div");
	var successResponseCallback = config.successResponseCallback || function () {};
	var errorResponseCallback = config.errorResponseCallback || function (err) { alert(err); };

	var enabled = true;
	var changed = false;
	var inputs = {};
	var inputsDiv = document.createElement("div");
	inputsDiv.setAttribute("class", "ajaxFormInputsDiv");

	var saveDiv = document.createElement("div");
	saveDiv.setAttribute("class", "ajaxFormSaveDiv");

	saveControlButton.onclick = save;

	var saveSuccessDiv = document.createElement("div");
	saveSuccessDiv.setAttribute("class", "ajaxFormSaveSuccessDiv");

	function printForm() {
		for (var fieldName in fields) {
			if (!values[fieldName]) {
				values[fieldName] = "";
			}

			var fieldDomType = "input";
			switch (fields[fieldName].type) {
				case "textarea": {
					fieldDomType = "textarea";
					break;
				}
				default: {
					break;
				}
			}
			var field = document.createElement(fieldDomType);
			field.setAttribute("class", "ajaxFormInput ajaxFormInput_" + fieldName);
			field.value = values[fieldName];
			field.onkeyup = createRefreshChangedState(fieldName);
			field.onclick = createRefreshChangedState(fieldName);
			field.onchange = createRefreshChangedState(fieldName);

			var inputDiv = document.createElement("div");
			inputDiv.setAttribute("class", "ajaxFormInputDiv");

			switch (fields[fieldName].type) {
				case "number": {
					field.setAttribute("type", "number");
					if (typeof fields[fieldName].min === "number") {
						field.min = fields[fieldName].min;
					}
					if (typeof fields[fieldName].max === "number") {
						field.max = fields[fieldName].max;
					}
					if (typeof fields[fieldName].step === "number") {
						field.step = fields[fieldName].step;
					}
					break;
				}
				case "textarea": {
					
					break;
				}
				case "hidden": {
					field.setAttribute("type", "hidden");
					break;
				}
				default: {
					field.setAttribute("type", "text");
				}
			}
			if (typeof fields[fieldName].disabled === "boolean") {
				field.disabled = fields[fieldName].disabled;
			}
			if (typeof fields[fieldName].placeHolder === "string") {
				field.setAttribute("placeHolder", fields[fieldName].placeHolder);
			}
			inputs[fieldName] = field;
			inputDiv.appendChild(field);
			inputsDiv.appendChild(inputDiv);
		}
		parentDiv.appendChild(inputsDiv);

		saveDiv.appendChild(saveControlButton);
		saveDiv.appendChild(saveSuccessTick);
		parentDiv.appendChild(saveDiv);

		refreshSaveControlButtonVisibility();
	}
	
	function refreshChangedState(fieldName) {
		if (inputs[fieldName].value !== values[fieldName]) {
			changed = true;
			refreshSaveControlButtonVisibility();
		}
	}

	function createRefreshChangedState(fieldName) {
		return function() {
			refreshChangedState(fieldName);
		}
	}

	function save() {
		if ((!enabled || !changed) && !showControlButtonAlways) {
			return;
		}
		
		// check requirements for field values
		for (var fieldName in fields) {
			if (typeof(fields[fieldName].checkFunction) === "function") {
				if (!fields[fieldName].checkFunction(inputs)) {
					return;
				}
			}
			if (fields[fieldName].required && inputs[fieldName].value === "") {
				errorResponseCallback("Required fields cannot be empty!");
				return;
			}
		}

		// generate POST query string
		var first = true;
		var postQueryString = "";
		for (var fieldName in inputs) {
			if (first) {
				first = false;
			} else {
				postQueryString += "&";
			}
			postQueryString += encodeURIComponent(fieldName) + "=" + encodeURIComponent(inputs[fieldName].value);
		}

		// send ajax request
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				var saveResponse;
				try {
					saveResponse = JSON.parse(this.responseText);
				} catch (err) {
					// invalid reply to AJAX request
					errorResponseCallback("Action failed due to invalid response from the server!");
					return;
				}

				if (saveResponse.error) {
					errorResponseCallback(saveResponse.error);
					return;
				}
				if (!saveResponse.OK) {
					errorResponseCallback("Action failed due to an unknown server error!");
					return;
				}
				changed = false;
				for (var fieldName in inputs) {
					values[fieldName] = inputs[fieldName].value;
				}
				successResponseCallback();
				refreshSaveControlButtonVisibility(true);
			}
		};
		xhttp.open("POST", saveUrl, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		xhttp.send(postQueryString);
	}

	function enable() {
		enabled = true;
		refreshSaveControlButtonVisibility();
		// TO DO: disable fields also
	}

	function disable() {
		enabled = false;
		refreshSaveControlButtonVisibility();
		// TO DO: enable fields also
	}

	function refreshSaveControlButtonVisibility(success) {
		if (success) {
			saveSuccessTick.style.display = "block";
		} else {
			saveSuccessTick.style.display = "none";
		}
		if (showControlButtonAlways) {
			saveControlButton.style.display = "block";
			return;
		}
		if (!enabled || !changed) {
			saveControlButton.style.display = "none";
			return;
		}
		saveControlButton.style.display = "block";
	}

	var obj = {
		printForm: printForm,
		save: save,
		enable: enable,
		disable: disable
	};
	return obj;
}