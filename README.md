Backstage Engine
================

# 1. Components and features

## 1.1. Form class

## 1.2. AjaxForm

## 1.3. DBE

## 1.4. List

### 1.4.1. Properties

### 1.4.2. Methods

#### 1.4.2.1. addColumn

Adds a single column to the list.

```php
$listInstance -> addColumn("columnName", [
	"label" => "column label",
	"orderBy" => true,
	"filterBy" => ["filter data 1", "filter data 2"],
	"filterCaption" => "filter caption"
]);
```

#### 1.4.2.2. addColumns

Adds an array of columns to the list.

```php
$listInstance -> addColumns([
	"columnName" => [
		"label" => "column label",
		"orderBy" => true,
		"filterBy" => ["filter data 1", "filter data 2"],
		"filterCaption" => "filter caption"
	],
	"column2" => [
		"label" => "column3 label"
	]
]);
```

#### 1.4.2.3. setColumns

Calls deleteColumns method first, addColumns method after.

#### 1.4.2.4. deleteColumns

Deletes columns from related arrays.

```php
$listInstance -> deleteColumns();
```

## 1.5. crudPage

### 1.5.1. Properties

Note: several properties are belonging to the inner implementation of the crudPage. You have the possibility to modify them, but it might be that you should not do that.

Prop                           | Type         | Default | Details
---                            | ---          | ---     | ---
itemDBE                        | DBE          | ---     | database element of the item, for creating, viewing and editing the element
handleSubmitErrorMessage       | string       | ""      | property containing error message when handling a submit operation on the itemDBE
idField                        | string       | ---     | id column name of the database table representing the corrsponding DB element. This value will be the id field received in the belonging POST requests too.
itemID                         | int          | 0       | id of the edited item (if there is an active edit request)
DBCondition                    | string       | mysql command part for filtering 'idField' column by 'itemId' value concatenated with the property 'additionalDBCondition' | filter condition for loading and updating the proper row in the DB element representing table in the database
additionalDBCondition          | string       | ""      | additional filter condition concatenated to the 'DBCondition' property by default
DBConditionValues              | assoc. array | ["crudPage_itemID" => item_ID] | values belonging to the prepared statement part 'DBCondition'
additionalDBConditionValues    | assoc. array | []      | values belonging to the prepared statement part 'additionalDBCondition'
DBConditionValuesMerged        | assoc. array | []      | merged DBConditionValues & additionalDBConditionalValues (for actual use)
itemName                       | string       | "item"  | caption for the item
itemList                       | felsorolas   | ---     | list instance for listing and deleting items belonging to the represented database entity
itemListTitle                  | string       | "Items" | title for the item list
itemDBEClass                   | string       | ---     | class name defining the type of the itemDBE (this class will be instantiated)
itemListClass                  | string       | ---     | class name defining the type of the itemListClass (this class will be instantiated)
labels                         | assoc. array | numerous label | labels: itemCreatedSuccessfully, itemEditedSuccessFully, newItemTitle, editItemTitle, editItemTitleItemID, itemDeletedSuccessfully, itemDeletedError, addNewItemLink, deleteConfirmQuestion
anchor                         | boolean      | false   | displaying anchored link at the top of the page refering the area of the page for creating new item
showItemIdInTitle              | boolean      | false   | if set to true, item ID will be displayed in the title of the item editing form
formListOrder                  | boolean      | true    | if set to true, ui components will be displayed in the next order: form first, then listing
caseImgButtons                 | assoc. array | edit and delete caseImgButtons | caseImgButtons, displayed in the listing and handled their belonging request (clicking on them) automatically in the 'printPage' method
editItemField                  | string       | "edit_" concatenated with the 'idField' | field name for the edit caseImgButton
deleteItemField                | string       | "delete_" concatenated with the 'idField' | field name for the delete caseImgButton
caseImgHandlersOnly            | boolean      | false   | caseImgButton handler for the delete requests set this automatically to true in order to prevent handling the request as an editing or printing the form and the list after the delete operation
additionalBackButtomFormValues | assoc. array | []      | additional values for the 'back' buttons of the itemDBE and the itemList

### 1.5.2. Methods

## 1.6. rsaEncrypt class

Class for basic RSA encryption and decryption using a generated RSA key pair.

### 1.6.1. Properties

Prop    | Type   | Default | Details
---     | ---    | ---     | ---
pubkey  | string | ""      | Public key in PEM format represented as string
privkey | string | ""      | Private key in PEM format represented as string

### 1.6.2. Methods

#### 1.6.2.1. encrypt

Returns the base64 encoded, encrypted data (partitioned to chunks if needed depending on data and key length). Uses the public key set in the 'pubkey' property.

```php
$encryptedData = $rsaEnryptionInstance -> encrypt("some data");
```

#### 1.6.2.2. decrypt

Returns the base64 decoded, decrypted data (partitioned chunks concatenated). Uses the private key set in the 'privkey' property.

```php
$decryptedData = $rsaEnryptionInstance -> decrypt($encryptedData);
```

### 1.6.3. Example

```php
$rsaEncryptionInstance = new rsaEncryption();
$rsaEncryptionInstance -> pubkey = file_get_contents("public.pem");
$rsaEncryptionInstance -> privkey = file_get_contents("private_unencrypted.pem");

try {
	$encryptedData = $rsaEncryptionInstance -> encrypt("some data")
	$decryptedData = $rsaEncryptionInstance -> decrypt($encryptedData);
	print $decryptedData;
} catch (Exception $e) {
	print $e -> getMessage();
}
```

# 2. Integrated libraries

## 2.1. swiftMailer

## 2.2. fPDF

## 2.3. jsModal

### 2.3.1. static content

```js
Modal.open({
	content: "<b>some static content</b>",
	draggable: false,
	title: "Some title"
});
```

### 2.3.2. ajax content

```php
?>
<script type="text/javascript">
Modal.open({
	ajaxContent: "<? print getUrlAjax("ajaxexample"); ?>",
	draggable: false,
	title: "Some title"
});
</script>
<?
```

### 2.3.3. ajax content with passed post data

```php
?>
<script type="text/javascript">
Modal.open({
	ajaxContent: "<? print getUrlAjax("ajaxexample"); ?>",
	ajaxPostData: {
		prop1: "value1",
		prop2: "value2"
	},
	draggable: false,
	title: "Some title"
});
</script>
<?
```
